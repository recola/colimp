program demo
  use coliflower, only: initcoli,D0,sp,dp,lp,qp,setmuir2,setmuuv2
  implicit none

  complex(kind=sp) :: d0res_sp
  complex(kind=dp) :: d0res_dp
  complex(kind=lp) :: d0res_lp
  complex(kind=qp) :: d0res_qp
  complex(kind=qp) :: p10_qp,p21_qp,p32_qp,p30_qp,p20_qp,p31_qp,m02_qp,m12_qp,m22_qp,m32_qp
  complex(kind=lp) :: p10_lp,p21_lp,p32_lp,p30_lp,p20_lp,p31_lp,m02_lp,m12_lp,m22_lp,m32_lp
  complex(kind=dp) :: p10_dp,p21_dp,p32_dp,p30_dp,p20_dp,p31_dp,m02_dp,m12_dp,m22_dp,m32_dp
  complex(kind=sp) :: p10_sp,p21_sp,p32_sp,p30_sp,p20_sp,p31_sp,m02_sp,m12_sp,m22_sp,m32_sp

  real(kind=sp) :: rescale_sp = 1.3_sp,acc_sp
  real(kind=dp) :: rescale_dp = 1.3_dp,acc_dp
  real(kind=lp) :: rescale_lp = 1.3_lp,acc_lp
  real(kind=qp) :: rescale_qp = 1.3_qp,acc_qp

  call set_kinematics(1)
  call set_masses(1)
  call run

  contains

  subroutine set_kinematics(setup)
    integer :: setup
    write(*,*) "Setting kinematics:", setup
    select case (setup)
      case (1)
        p10_qp = 0._qp
        p21_qp = 284842.09968989424_qp
        p32_qp = 0._qp
        p30_qp = 146892.67583825623_qp
        p20_qp = -86930.363195152982_qp
        p31_qp = -481334.86127669609_qp
      case (2)
        p10_qp = 753721.99320694886_qp
        p21_qp = 0._qp
        p32_qp = 0._qp
        p30_qp = 33074.837261271568_qp
        p20_qp = -352477.03417751601_qp
        p31_qp = 1210000.0000000019_qp
      case (3)
        p10_qp = 0._qp
        p21_qp = 344658.94062477211_qp
        p32_qp = 0._qp
        p30_qp = 177740.13776429006_qp
        p20_qp = -105185.73946613508_qp
        p31_qp = -582415.18214480230_qp
      case (4)
        p10_qp=309223.78866098239_qp
        p21_qp=6463.9992010000005_qp
        p32_qp=0.0000000000000000_qp
        p30_qp=6463.9992010000005_qp
        p20_qp=67721.627924234112_qp
        p31_qp=43600.429513300907_qp
      case (5)
        p10_qp = 2604256.8751853532_qp
        p21_qp = 168999999.99999994_qp
        p32_qp = 29584.000000000000_qp
        p30_qp = 9404933.1593555138_qp
        p20_qp = 48454975.835252516_qp
        p31_qp = 128545153.80379194_qp
      case (6)
        p10_qp = 10826421.217222791_qp
        p21_qp = 169000000.00000000_qp
        p32_qp = 29584.000000000000_qp
        p30_qp = 459202.33460163383_qp
        p20_qp = 63638007.170090415_qp
        p31_qp = 93391315.164123848_qp
      case (7)
        p10_qp = -25247.3101652846235083416104317_qp
        p21_qp = 0._qp
        p32_qp = 0._qp
        p30_qp =  8417.41955341014545410871505737_qp
        p20_qp = -3767.37290906944872403983026743_qp
        p31_qp =  50246.3629887908609816804528236_qp
      case (8)
        p10_qp = -218573.927898838782532308242294_qp
        p21_qp = 0._qp
        p32_qp = 0._qp
        p30_qp = 15625._qp
        p20_qp = -104276.782627973513744446672677_qp
        p31_qp = -131437.389342469882671173762261_qp
    end select

  end subroutine set_kinematics

  subroutine set_masses(setup)
    integer :: setup
    write(*,*) "Setting masses:    ", setup
    select case (setup)
      case (1)
        m02_qp = 0._qp
        m12_qp = 0._qp
        m22_qp = 0._qp
        m32_qp = 0._qp
      case (2)
        m02_qp = 300._qp
        m12_qp = 300._qp
        m22_qp = 300._qp
        m32_qp = 300._qp
      case (3)
        m02_qp = 0._qp
        m12_qp = 300._qp
        m22_qp = 0._qp
        m32_qp = 300._qp
      case (4)
        m02_qp = 300._qp
        m12_qp = 0._qp
        m22_qp = 0._qp
        m32_qp = 300._qp
      case (5)
        m02_qp = 300._qp
        m12_qp = 0._qp
        m22_qp = 300._qp
        m32_qp = 0._qp
      case (6)
        m02_qp = 0._qp
        m12_qp = 20._qp
        m22_qp = 300._qp
        m32_qp = 4000._qp
      case (7)
        m02_qp = 29584._qp
        m12_qp = 29584._qp
        m22_qp = 29584._qp
        m32_qp = 29584._qp
    end select

  end subroutine set_masses


  subroutine run()

    p10_sp = p10_qp
    p21_sp = p21_qp
    p32_sp = p32_qp
    p30_sp = p30_qp
    p20_sp = p20_qp
    p31_sp = p31_qp
    m02_sp = m02_qp
    m12_sp = m12_qp
    m22_sp = m22_qp
    m32_sp = m32_qp

    p10_dp = p10_qp
    p21_dp = p21_qp
    p32_dp = p32_qp
    p30_dp = p30_qp
    p20_dp = p20_qp
    p31_dp = p31_qp
    m02_dp = m02_qp
    m12_dp = m12_qp
    m22_dp = m22_qp
    m32_dp = m32_qp

    p10_lp = p10_qp
    p21_lp = p21_qp
    p32_lp = p32_qp
    p30_lp = p30_qp
    p20_lp = p20_qp
    p31_lp = p31_qp
    m02_lp = m02_qp
    m12_lp = m12_qp
    m22_lp = m22_qp
    m32_lp = m32_qp

    call initcoli

    d0res_sp = D0(p10_sp,p21_sp,p32_sp,p30_sp,p20_sp,p31_sp,m02_sp,m12_sp,m22_sp,m32_sp)
    d0res_dp = D0(p10_dp,p21_dp,p32_dp,p30_dp,p20_dp,p31_dp,m02_dp,m12_dp,m22_dp,m32_dp)
    d0res_lp = D0(p10_lp,p21_lp,p32_lp,p30_lp,p20_lp,p31_lp,m02_lp,m12_lp,m22_lp,m32_lp)
    d0res_qp = D0(p10_qp,p21_qp,p32_qp,p30_qp,p20_qp,p31_qp,m02_qp,m12_qp,m22_qp,m32_qp)
    write(*,*)
    write(*,*) "D0 in sp:", d0res_sp
    write(*,*) "D0 in dp:", d0res_dp
    write(*,*) "D0 in lp:", d0res_lp
    write(*,*) "D0 in qp:", d0res_qp
    write(*,*)


    call setmuuv2(rescale_sp**2)
    call setmuir2(rescale_sp**2)
    call setmuuv2(rescale_dp**2)
    call setmuir2(rescale_dp**2)
    call setmuuv2(rescale_lp**2)
    call setmuir2(rescale_lp**2)
    call setmuuv2(rescale_qp**2)
    call setmuir2(rescale_qp**2)

    p10_qp = p10_qp*rescale_qp**2
    p21_qp = p21_qp*rescale_qp**2
    p32_qp = p32_qp*rescale_qp**2
    p30_qp = p30_qp*rescale_qp**2
    p20_qp = p20_qp*rescale_qp**2
    p31_qp = p31_qp*rescale_qp**2
    m02_qp = m02_qp*rescale_qp**2
    m12_qp = m12_qp*rescale_qp**2
    m22_qp = m22_qp*rescale_qp**2
    m32_qp = m32_qp*rescale_qp**2

    p10_sp = p10_qp
    p21_sp = p21_qp
    p32_sp = p32_qp
    p30_sp = p30_qp
    p20_sp = p20_qp
    p31_sp = p31_qp
    m02_sp = m02_qp
    m12_sp = m12_qp
    m22_sp = m22_qp
    m32_sp = m32_qp

    p10_dp = p10_qp
    p21_dp = p21_qp
    p32_dp = p32_qp
    p30_dp = p30_qp
    p20_dp = p20_qp
    p31_dp = p31_qp
    m02_dp = m02_qp
    m12_dp = m12_qp
    m22_dp = m22_qp
    m32_dp = m32_qp

    p10_lp = p10_qp
    p21_lp = p21_qp
    p32_lp = p32_qp
    p30_lp = p30_qp
    p20_lp = p20_qp
    p31_lp = p31_qp
    m02_lp = m02_qp
    m12_lp = m12_qp
    m22_lp = m22_qp
    m32_lp = m32_qp

    acc_qp = D0(p10_qp,p21_qp,p32_qp,p30_qp,p20_qp,p31_qp,m02_qp,m12_qp,m22_qp,m32_qp)/d0res_qp*rescale_qp**4-1
    write(*,*) "qp accuracy rescaling:", nint(-log10(abs(acc_qp)))
    write(*,*)

    acc_sp = D0(p10_sp,p21_sp,p32_sp,p30_sp,p20_sp,p31_sp,m02_sp,m12_sp,m22_sp,m32_sp)/d0res_sp*rescale_sp**4-1
    write(*,*) "sp accuracy rescaling:", nint(-log10(abs(acc_sp)))
    acc_sp = d0res_sp/d0res_qp - 1
    write(*,*) "sp compared to qp:    ",  nint(-log10(abs(acc_sp)))



    acc_dp = D0(p10_dp,p21_dp,p32_dp,p30_dp,p20_dp,p31_dp,m02_dp,m12_dp,m22_dp,m32_dp)/d0res_dp*rescale_dp**4-1
    write(*,*) "dp accuracy rescaling:", nint(-log10(abs(acc_dp)))
    acc_dp = d0res_dp/d0res_qp - 1
    write(*,*) "dp compared to qp:    ",  nint(-log10(abs(acc_dp)))

    acc_lp = D0(p10_lp,p21_lp,p32_lp,p30_lp,p20_lp,p31_lp,m02_lp,m12_lp,m22_lp,m32_lp)/d0res_lp*rescale_lp**4-1
    write(*,*) "lp accuracy rescaling:", nint(-log10(abs(acc_lp)))
    acc_lp = d0res_lp/d0res_qp - 1
    write(*,*) "lp compared to qp:    ",  nint(-log10(abs(acc_lp)))

  end subroutine run

end program
