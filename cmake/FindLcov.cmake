# Copyright (c) 2020, Jean-Nicolas Lang, jlang@physik.uzh.ch
# -----------------------------------------------------------------------------

find_program(LCOV_EXECUTABLE
             NAMES lcov
             DOC "Path to lcov executable")
find_program(GENHTML_EXECUTABLE
             NAMES genhtml
             DOC "Path to genhtml executable (part of lcov)")

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(Lcov
                                  "Failed to find lcov executable"
                                  LCOV_EXECUTABLE)
