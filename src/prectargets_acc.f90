#import "prectargets_acc.inc"

module prectargets_acc_<T>
  implicit none
  real(kind=8), parameter :: acc = @T
end module prectargets_acc_<T>
