!***********************************************************************
!                                                                      *
!     Scalar 1-function                                                *
!                                                                      *
!***********************************************************************
! functions:                                                           *
! A0_coli                                                              *
#import "prectemplate.inc"

module coli_a0_<T>
  use params_coli_<T>
  use common_coli_<T>
  use checkparams_coli
  use coli_aux_<T>, only: elimminf2_coli
  implicit none
  contains

!***********************************************************************
  function A0_<T>(m2) result (A0)
!***********************************************************************
!     scalar one-loop-integral A0_coli                                 *
!-----------------------------------------------------------------------
!     02.10.89 Ansgar Denner      last changed 24.10.04                *
!***********************************************************************
  implicit   none
  complex(kind=prec) :: A0,m2

  if (m2 .eq. rnul) then
    A0 = rnul
  else if (elimminf2_coli(m2) .eq. rnul) then
    A0 = rnul
  else
    A0 = m2 * ( rone - log(m2/muuv2) )
  endif
#ifdef SING
  A0 = A0 + m2 * deltauv
#endif
  end

end module coli_a0_<T>
