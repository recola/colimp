!***********************************************************************
!                                                                      *
!     auxiliary functions for COLI library                             *
!                                                                      *
!***********************************************************************
!                                                                      *
!     last changed  09.11.12  Ansgar Denner                            *
!     errorflags    22.05.13  Ansgar Denner                            *
!                   04.06.13  chdet, chinv moved here                  *
!                   30.03.15  irratterms included                      *
!                                                                      *
!***********************************************************************
! subroutines:                                                         *
! initcoli,initcoli_in_collier                                         *
! setmuuv2_coli, setmuir2_coli, setdeltauv_coli, setdeltair_coli       *
! getmuuv2_coli, getmuir2_coli, getdeltauv_coli, getdeltair_coli       *
! setshiftms2_coli, getshiftms2_coli                                   *
! setminf2_coli, clearcoliminf2                                        *
! setminfscale2_coli, getminfscale2_coli, modminfscale2_coli           *
! setirratterms_coli, unsetirratterms_coli                             *
! setprecpars_coli                                                     *
! setinfo_coli, unsetinfo_coli                                         *
! chinv                                                                *
! functions:                                                           *
! cln_coli, cspenc_coli, cspenh_coli, cspcon_coli, cspcos_coli         *
! csp2con_coli, csp2cos_coli, csp3con_coli, csp4con_coli               *
! eta2_coli, eta2s_coli                                                *
! chdet                                                                *
! minfscaledown_coli, minfscaledown2_coli                              *
! elimminf_coli, elimminf2_coli                                        *
!***********************************************************************
#import "prectemplate.inc"

!  <14-01-21, J.-N. Lang> !
! extended cspenh_coli to include more terms in the expansion
module coli_aux_<T>
  use common_coli_<T>
  use params_coli_<T>
  implicit none
  contains

  subroutine initcoli_<T>
    use coli_version, only: print_logo
    implicit none
    call print_logo
    call setmuuv2_coli(rone)
    call setmuir2_coli(rone)
    call setminfscale2_coli(rone)
    call setshiftms2_coli(rnul)
    call setIRRatTerms_coli

    ErrCnt_coli = 0
    erroutlev_coli = 1
    ErrFlag_coli = 0
    ir_rat_terms=.true.

    ncoliminf = 0
    coliminf(:) = rnul
    coliminf2(:) = rnul
    coliminffix(:) = rnul
    coliminffix2(:) = rnul
    coliminfscale=rone
    coliminfscale2=rone

  end subroutine initcoli_<T>


!***********************************************************************
  function cln_fast_coli(z)
!***********************************************************************
!     complex logarithm of z                                           *
!----------------------------------------------------------------------*
!     02.06.20 Alexander Voigt                                         *
!***********************************************************************
  implicit none
  real(kind=prec)    :: norm2,arg,re,im
  complex(kind=prec) :: cln_fast_coli,z

  re = real(z)
  im = aimag(z)

  norm2 = re*re + im*im
  arg = atan2(im, re)

  cln_fast_coli = cmplx(rhlf*log(norm2),arg,kind=prec)

  end

!***********************************************************************
  function cln_coli(cz,eps)
!***********************************************************************
!     complex logarithm of cz + i*eps                                  *
!----------------------------------------------------------------------*
!     09.01.90 Ansgar Denner         last changed 15.03.04             *
!***********************************************************************
  implicit none
  complex(kind=prec) :: cln_coli,cz
  real(kind=prec)    :: eps
  logical :: errorwriteflag

  if(abs(aimag(cz)).gt.impacc*abs(real(cz)).or.real(cz).gt.rnul) then
    cln_coli=cln_fast_coli(cz)
  elseif(eps.ne.0) then
    cln_coli=cln_fast_coli(-cz)+pi*cima*sign(rone,eps)
  elseif(aimag(cz).ne.rnul) then
    cln_coli=cln_fast_coli(cz)
  else
    cln_coli=cln_fast_coli(-cz)+pi*cima
  endif

  end

!***********************************************************************
  function cspenc_coli(cz,eps)
!***********************************************************************
!       complex spence function  of cz + i*eps                         *
!       calculated by mapping on the area where there is a quickly     *
!       convergent series                                              *
!----------------------------------------------------------------------*
!     08.01.90 Ansgar Denner        last changed  3.02.97 ad           *
!***********************************************************************
  implicit none
  complex(kind=prec) :: cspenc_coli,cz
  real(kind=prec)    :: eps
  real(kind=prec)    :: az,rz,az1
  complex(kind=prec) :: cz1
  logical :: errorwriteflag

  cz1 = rone-cz
  az1 = abs(cz1)
  az  = abs(cz)
  rz  = real(cz)


  if (az1.lt.calacc) then
     cspenc_coli = pi2_6
  else if (rz.lt.rhlf) then
     if (az.lt.rone) then
        cspenc_coli =  cspenh_coli(cz,eps)
     else
        cspenc_coli = -pi2_6 - rhlf*cln_coli(-cz,-eps)**2 &
                      - cspenh_coli(rone/cz,-eps)
     endif
  else
     if (az1.lt.rone) then
        cspenc_coli =  pi2_6 - cln_coli(cz,eps)*cln_coli(cz1,-eps) &
                       - cspenh_coli(cz1,-eps)
     else
        cspenc_coli = rtwo*pi2_6 + rhlf*cln_coli(-cz1,-eps)**2 &
                      - cln_coli(cz,eps)*cln_coli(cz1,-eps) &
                      +  cspenh_coli(rone/cz1,eps)
     endif
  endif
  end

!***********************************************************************
  function cspenh_coli(cz,eps)
!***********************************************************************
!       complex spence function of cz + i*eps                          *
!       in convergence region                                          *
!       calculation of bernoulli series                                *
!----------------------------------------------------------------------*
!     09.01.90 Ansgar Denner        last changed 02.09.08              *
!***********************************************************************
  implicit none
  complex(kind=prec) :: cspenh_coli,cz,x,x2
  real(kind=prec)    :: eps
  integer            :: j
  real(kind=prec)    ::     factor
  complex(kind=prec) :: power,term,csp
  logical            :: errorwriteflag

  real(kind=prec), parameter :: b(26) = [ &
  0.0555555555555555555555555555555555556_prec, &
  -0.00222222222222222222222222222222222222_prec, &
  0.000226757369614512471655328798185941043_prec, &
  -0.0000352733686067019400352733686067019400_prec, &
   7.28788607576486364365152243940122728_prec*10._prec**(-6), &
  -1.87304216608245912275216304520333824_prec*10._prec**(-6), &
   5.75556131111686667242222797778353334_prec*10._prec**(-7), &
  -2.05811816730694085557599590663796738_prec*10._prec**(-7), &
   8.39601906252019667653695964928121687_prec*10._prec**(-8), &
  -3.84836926577493085861451814957087348_prec*10._prec**(-8), &
   1.95808179510841530557165072614648901_prec*10._prec**(-8), &
  -1.09514111576435850940191979156020639_prec*10._prec**(-8), &
   6.67820767995953098574723733154104389_prec*10._prec**(-9), &
  -4.40985161879723756668598448117179992_prec*10._prec**(-9), &
   3.13488632641969892554308404883700396_prec*10._prec**(-9), &
  -2.38704052887514413487556592692239075_prec*10._prec**(-9), &
   1.93831728498995765733457426115334518_prec*10._prec**(-9), &
  -1.67199106751401582156375156107568109_prec*10._prec**(-9), &
   1.52684492488019489376214232149159631_prec*10._prec**(-9), &
  -1.47155308927870396432592547358358807_prec*10._prec**(-9), &
   1.49272872310711324955736230878523087_prec*10._prec**(-9), &
  -1.58975343181854300101914547478291275_prec*10._prec**(-9), &
   1.77354626479481385784461436127095023_prec*10._prec**(-9), &
  -2.06835842921446891145221503533508275_prec*10._prec**(-9), &
   2.51687688431033845175611441333870702_prec*10._prec**(-9), &
  -3.19006759500453529598466535929886728_prec*10._prec**(-9)]
  x      =  -cln_coli(rone-cz,-eps)
  x2     =  x*x
  power  =  x
  factor =  rone
  cspenh_coli =  x - x2/4
  do j=2,52,2
     factor = factor / j
     power  = power * x2
     term   = b(j/2) * factor * power
     csp    = cspenh_coli + term
     if (csp.eq.cspenh_coli) return
     cspenh_coli = csp
  end do
  end


!***********************************************************************
  function cspcon_coli(z1,z2,pz,ompz,i1,i2)
!***********************************************************************
!  complex spence function   sp(1-z1*z2)                               *
!                          plus continuation terms                     *
!  pz = z1*z2, ompz = 1-pz                                             *
!  i1,i2 determine signs of infinitesimal imaginary parts of           *
!  z1,z2                                                               *
!----------------------------------------------------------------------*
!  16.10.08 Ansgar Denner      last changed 09.11.12                   *
!***********************************************************************
  implicit none
  complex(kind=prec) :: z1,z2,pz,ompz
  real(kind=prec)    :: i1,i2,ip
  complex(kind=prec) :: cspcon_coli,sumln
  logical :: errorwriteflag

! to ensure correct continuation for infinitesimal imaginary parts
  ip=-sign(rone,i1)
  if(pz.eq.rnul)then
    cspcon_coli = pi2_6
  else
    sumln=cln_coli(z1,i1)+cln_coli(z2,i2)
    if(abs(pz).lt.rone) then
      if(real(pz).gt.rhlf) then
        cspcon_coli = cspenc_coli(rone-pz,-ip)+cln_coli(rone-pz,-ip) &
            *(cln_coli(pz,ip)-sumln)
      else
        cspcon_coli = pi2_6-cspenc_coli(pz,ip) &
            -cln_coli(rone-pz,-ip)*sumln
      endif
    elseif(pz.ne.rone) then
      if(real(pz).lt.rtwo) then
        cspcon_coli = -cspenc_coli(rone-rone/pz,ip) &
            +cln_coli(rone-rone/pz,ip) &
            *(cln_coli(pz,ip)-sumln) &
            -rhlf*(sumln)**2
      else
        cspcon_coli = -pi2_6+cspenc_coli(rone/pz,-ip) &
            -cln_coli(rone-rone/pz,ip)*sumln &
            -rhlf*(sumln)**2
      endif
    else
      cspcon_coli =rnul
    endif
  endif

  end

!***********************************************************************
  function cspcos_coli(z1,z2,i1,i2)
!***********************************************************************
!  complex spence function   sp(1-z1*z2)                               *
!                          plus continuation terms                     *
!  i1,i2 determine signs of infinitesimal imaginary parts of           *
!  z1,z2                                                               *
!----------------------------------------------------------------------*
!  30.04.08 Ansgar Denner      last changed 28.10.08                   *
!***********************************************************************
  implicit none
  complex(kind=prec) :: z1,z2,pz,ompz
  real(kind=prec)    :: i1,i2,ip
  complex(kind=prec) :: cspcos_coli,sumln
  logical :: errorwriteflag


100  format(a21,8g25.17)
  pz=z1*z2
  ompz=rone-pz
! to ensure correct continuation for infinitesimal imaginary parts
  if(i2.ne.rnul)then
    ip = i2*sign(rone,real(z1))
  else
    ip = i1*sign(rone,real(z2))
  endif
  if(pz.eq.rnul)then
    cspcos_coli = pi2_6
  else
    sumln=cln_coli(z1,i1)+cln_coli(z2,i2)
    if(abs(pz).lt.rone) then
      if(real(pz).gt.rhlf) then
        cspcos_coli = cspenc_coli(rone-pz,-ip)+cln_coli(rone-pz,-ip) &
            *(cln_coli(pz,ip)-sumln)
      else
        cspcos_coli = pi2_6-cspenc_coli(pz,ip) &
            -cln_coli(rone-pz,-ip)*sumln
      endif
    elseif(pz.ne.rone) then
      if(real(pz).lt.rtwo) then
        cspcos_coli = -cspenc_coli(rone-rone/pz,ip) &
            +cln_coli(rone-rone/pz,ip) &
            *(cln_coli(pz,ip)-sumln) &
            -rhlf*(sumln)**2
      else
        cspcos_coli = -pi2_6+cspenc_coli(rone/pz,-ip) &
            -cln_coli(rone-rone/pz,ip)*sumln &
            -rhlf*(sumln)**2
      endif
    else
      cspcos_coli =rnul
    endif
  endif

  end


!!***********************************************************************
!      function csp2con_coli(z1,z2,pz,ompz,i1,i2)
!!***********************************************************************
!!  complex spence function   sp(1-z1*z2)                               *
!!                          plus continuation terms                     *
!!  pz = z1*z2, ompz = 1-pz                                             *
!!  i1,i2 determine signs of infinitesimal imaginary parts of           *
!!  z1,z2                                                               *
!!----------------------------------------------------------------------*
!!  30.04.08 Ansgar Denner      last changed 03.06.08                   *
!!***********************************************************************
!      use params_coli
!      use checkparams_coli
!      implicit none
!      complex(kind=prec) :: z1,z2,pz,ompz
!      real(kind=prec) ::     i1,i2,ip
!      complex(kind=prec) :: csp2con_coli,cspenc_coli,cln_coli,sumln
!      logical    errorwriteflag
!
!
!      ip=rone
!      if(pz.eq.rnul)then
!        csp2con_coli = pi2_6
!      else
!        sumln=cln_coli(z1,i1)+cln_coli(z2,i2)
!        if(abs(pz).lt.rone) then
!          if(real(pz).gt.rhlf) then
!            csp2con_coli = cspenc_coli(rone-pz,-ip)+cln_coli(rone-pz,-ip) &
!                *(cln_coli(pz,ip)-sumln)
!          else
!            csp2con_coli = pi2_6-cspenc_coli(pz,ip) &
!                -cln_coli(rone-pz,-ip)*sumln
!          endif
!        elseif(pz.ne.rone) then
!          if(real(pz).lt.rtwo) then
!#ifdef WIB
!            csp2con_coli = -cspenc_coli(rone-rone/pz,ip) &
!                +cln_coli(rone-rone/pz,ip) &
!                *(cln_coli(pz,ip)-sumln) &
!                -.5d0*(sumln)**2
!#else
!            csp2con_coli = -cspenc_coli(rone-rone/pz,ip) &
!                +cln_coli(rone-pz,-ip) &
!                *(cln_coli(pz,ip)-sumln) &
!              -.5d0*(cln_coli(pz,ip))**2
!#endif
!          else
!#ifdef WIB
!            csp2con_coli = -pi2_6+cspenc_coli(rone/pz,-ip) &
!                -cln_coli(rone-rone/pz,ip)*sumln &
!                -.5d0*(sumln)**2
!#else
!            csp2con_coli = rtwo*pi2_6+cspenc_coli(rone/pz,-ip) &
!                -cln_coli(rone-pz,-ip)*sumln &
!              +.5d0*(cln_coli(-pz,-ip))**2
!#endif
!          endif
!        else
!          csp2con_coli =rnul
!        endif
!      endif
!
!
!
!      end
!!***********************************************************************
!      function csp2cos_coli(z1,z2,i1,i2)
!!***********************************************************************
!!  complex spence function   sp(1-z1*z2)                               *
!!                          plus continuation terms                     *
!!  i1,i2 determine signs of infinitesimal imaginary parts of           *
!!  z1,z2                                                               *
!!----------------------------------------------------------------------*
!!  30.04.08 Ansgar Denner      last changed 22.09.08                   *
!!***********************************************************************
!      use params_coli
!      use checkparams_coli
!      implicit none
!      complex(kind=prec) :: z1,z2,pz,ompz
!      real(kind=prec) ::     i1,i2,ip
!      complex(kind=prec) :: csp2cos_coli,cspenc_coli,cln_coli,sumln
!
!
! 100  format(a21,8g25.17)
!      pz=z1*z2
!      ompz=rone-pz
!      ip=rone
!      if(pz.eq.rnul)then
!        csp2cos_coli = pi2_6
!      else
!        sumln=cln_coli(z1,i1)+cln_coli(z2,i2)
!        if(abs(pz).lt.rone) then
!          if(real(pz).gt.rhlf) then
!            csp2cos_coli = cspenc_coli(rone-pz,-ip)+cln_coli(rone-pz,-ip) &
!                *(cln_coli(pz,ip)-sumln)
!          else
!            csp2cos_coli = pi2_6-cspenc_coli(pz,ip) &
!                -cln_coli(rone-pz,-ip)*sumln
!          endif
!        elseif(pz.ne.rone) then
!          if(real(pz).lt.rtwo) then
!#ifdef WIB
!            csp2cos_coli = -cspenc_coli(rone-rone/pz,ip) &
!                +cln_coli(rone-rone/pz,ip) &
!                *(cln_coli(pz,ip)-sumln) &
!                -.5d0*(sumln)**2
!#else
!            csp2cos_coli = -cspenc_coli(rone-rone/pz,ip) &
!                +cln_coli(rone-pz,-ip) &
!                *(cln_coli(pz,ip)-sumln) &
!              -.5d0*(cln_coli(pz,ip))**2
!#endif
!          else
!#ifdef WIB
!            csp2cos_coli = -pi2_6+cspenc_coli(rone/pz,-ip) &
!                -cln_coli(rone-rone/pz,ip)*sumln &
!                -.5d0*(sumln)**2
!#else
!            csp2cos_coli = rtwo*pi2_6+cspenc_coli(rone/pz,-ip) &
!                -cln_coli(rone-pz,-ip)*sumln &
!              +.5d0*(cln_coli(-pz,-ip))**2
!#endif
!          endif
!        else
!          csp2cos_coli =rnul
!          if(sumln.ne.cd0) write(nerrout_coli,*) &
!              'csp2cos_coli: continuation possibly wrong'
!        endif
!      endif
!
!
!
!      end
!
!!***********************************************************************
!      function csp3con_coli(z1,z2,z3,pz,ompz,i1,i2,i3)
!!***********************************************************************
!!  complex spence function   sp(1-z1*z2*z3)                            *
!!                          plus continuation terms                     *
!!  pz = z1*z2*z3, ompz = 1-pz                                          *
!!  i1,i2,i3 determine signs of infinitesimal imaginary parts of        *
!!  z1,z2,z3                                                            *
!!----------------------------------------------------------------------*
!!  30.04.08 Ansgar Denner      last changed 03.06.08                   *
!!***********************************************************************
!      use params_coli
!      use checkparams_coli
!      implicit none
!      complex(kind=prec) :: z1,z2,z3,pz,ompz
!      real(kind=prec) ::     i1,i2,i3,ip
!      complex(kind=prec) :: csp3con_coli,cspenc_coli,cln_coli,sumln
!      logical    errorwriteflag
!
!      ip=rone
!      if(pz.eq.rnul)then
!        csp3con_coli = pi2_6
!      else
!        sumln=cln_coli(z1,i1)+cln_coli(z2,i2)+cln_coli(z3,i3)
!        if(abs(pz).lt.rone) then
!          if(real(pz).gt.rhlf) then
!            csp3con_coli = cspenc_coli(rone-pz,-ip)+cln_coli(rone-pz,-ip) &
!                *(cln_coli(pz,ip)-sumln)
!          else
!            csp3con_coli = pi2_6-cspenc_coli(pz,ip) &
!                -cln_coli(rone-pz,-ip)*sumln
!          endif
!        elseif(pz.ne.(rone,rnul)) then
!          if(real(pz).lt.rtwo) then
!#ifdef WIB
!            csp3con_coli = -cspenc_coli(rone-rone/pz,ip) &
!                +cln_coli(rone-rone/pz,ip) &
!                *(cln_coli(pz,ip)-sumln) &
!                -.5d0*(sumln)**2
!#else
!            csp3con_coli = -cspenc_coli(rone-rone/pz,ip) &
!                +cln_coli(rone-pz,-ip) &
!                *(cln_coli(pz,ip)-sumln) &
!              -.5d0*(cln_coli(pz,ip))**2
!#endif
!          else
!#ifdef WIB
!            csp3con_coli = -pi2_6+cspenc_coli(rone/pz,-ip) &
!                -cln_coli(rone-rone/pz,ip)*sumln &
!                -.5d0*(sumln)**2
!#else
!            csp3con_coli = rtwo*pi2_6+cspenc_coli(rone/pz,-ip) &
!                -cln_coli(rone-pz,-ip)*sumln &
!              +.5d0*(cln_coli(-pz,-ip))**2
!#endif
!          endif
!        else
!          csp3con_coli =rnul
!        endif
!      endif
!
!
!
!      end
!
!!***********************************************************************
!      function csp4con_coli(z1,z2,z3,z4,pz,ompz,i1,i2,i3,i4)
!!***********************************************************************
!!  complex spence function   sp(1-z1*z2*z3)                            *
!!                          plus continuation terms                     *
!!  pz = z1*z2*z3*z4, ompz = 1-pz                                       *
!!  i1,i2,i3,i4 determine signs of infinitesimal imaginary parts of     *
!!  z1,z2,z3,z4                                                         *
!!----------------------------------------------------------------------*
!!  02.05.08 Ansgar Denner      last changed 03.06.08                   *
!!***********************************************************************
!      use params_coli
!      use checkparams_coli
!      implicit none
!      complex(kind=prec) :: z1,z2,z3,z4,pz,ompz
!      real(kind=prec) ::     i1,i2,i3,i4,ip
!      complex(kind=prec) :: csp4con_coli,cspenc_coli,cln_coli,sumln
!      logical    errorwriteflag
!
!      ip=rone
!      if(pz.eq.rnul)then
!         csp4con_coli =pi2_6
!      else
!        sumln=cln_coli(z1,i1)+cln_coli(z2,i2) &
!            +cln_coli(z3,i3)+cln_coli(z4,i4)
!        if(abs(pz).lt.rone) then
!          if(real(pz).gt.rhlf) then
!            csp4con_coli = cspenc_coli(rone-pz,-ip)+cln_coli(rone-pz,-ip) &
!                *(cln_coli(pz,ip)-sumln)
!          else
!            csp4con_coli = pi2_6-cspenc_coli(pz,ip) &
!                -cln_coli(rone-pz,-ip)*sumln
!          endif
!        elseif(pz.ne.(rone,rnul)) then
!          if(real(pz).lt.rtwo) then
!#ifdef WIB
!            csp4con_coli = -cspenc_coli(rone-rone/pz,ip) &
!                +cln_coli(rone-rone/pz,ip) &
!                *(cln_coli(pz,ip)-sumln) &
!              -.5d0*(sumln)**2
!#else
!            csp4con_coli = -cspenc_coli(rone-rone/pz,ip) &
!                +cln_coli(rone-pz,-ip) &
!                *(cln_coli(pz,ip)-sumln) &
!              -.5d0*(cln_coli(pz,ip))**2
!#endif
!          else
!#ifdef WIB
!            csp4con_coli = -pi2_6+cspenc_coli(rone/pz,-ip) &
!                -cln_coli(rone-rone/pz,ip)*sumln &
!                -.5d0*(sumln)**2
!#else
!            csp4con_coli = rtwo*pi2_6+cspenc_coli(rone/pz,-ip) &
!                -cln_coli(rone-pz,-ip)*sumln &
!              +.5d0*(cln_coli(-pz,-ip))**2
!#endif
!          endif
!        else
!          csp4con_coli =rnul
!        endif
!      endif
!
!
!      end

!***********************************************************************
      function eta2_coli(z1,z2,pz,i1,i2,ip)
!***********************************************************************
!     complex eta-function with explicit infinitesimal im parts        *
!     pz = z1*z2                                                      *
!----------------------------------------------------------------------*
!     06.10.08    Ansgar Denner       last changed   06.10.08          *
!***********************************************************************
      implicit none
      complex(kind=prec) :: eta2_coli,z1,z2,pz
      real(kind=prec) ::     i1,i2,ip
      real(kind=prec) ::     im1,im2,imp,re1,re2,rep
      logical    errorwriteflag

      im1   = aimag(z1)
      im2   = aimag(z2)
      imp   = aimag(pz)
      re1   = real(z1)
      re2   = real(z2)
      rep   = real(pz)


      if (abs(im1).lt.impacc*abs(re1)) im1 = rnul
      if (abs(im2).lt.impacc*abs(re2)) im2 = rnul
      if (abs(imp).lt.impacc*abs(rep)) imp = rnul


      if (im1.ne.rnul.or.im2.ne.rnul.or.imp.ne.rnul) then
        if(im1.eq.rnul) im1 = i1
        if(im2.eq.rnul) im2 = i2
        if(imp.eq.rnul) imp = ip
        if(im1.lt.rnul.and.im2.lt.rnul.and.imp.gt.rnul) then
          eta2_coli = rtwo*pi*cima
        else if (im1.gt.rnul.and.im2.gt.rnul.and.imp.lt.rnul) then
          eta2_coli = -rtwo*pi*cima
        else
          eta2_coli = cnul
        endif
      else
        eta2_coli = cnul
        if (re1.lt.rnul.and.re2.lt.rnul) then
          if(i1.gt.rnul.and.i2.gt.rnul) then
            eta2_coli = -rtwo*pi*cima
          else   if(i1.lt.rnul.and.i2.lt.rnul) then
            eta2_coli = rtwo*pi*cima
          endif
        else if (rep.lt.rnul.and.re1.lt.rnul) then
          if(i1.gt.rnul.and.ip.lt.rnul) then
            eta2_coli = -rtwo*pi*cima
          else   if(i1.lt.rnul.and.ip.gt.rnul) then
            eta2_coli = rtwo*pi*cima
          endif
        else if (rep.lt.rnul.and.re2.lt.rnul) then
          if(i2.gt.rnul.and.ip.lt.rnul) then
            eta2_coli = -rtwo*pi*cima
          else   if(i2.lt.rnul.and.ip.gt.rnul) then
            eta2_coli = rtwo*pi*cima
          endif
        endif
      endif


      end

!***********************************************************************
      function eta2s_coli(z1,z2,i1,i2,ip)
!***********************************************************************
!     complex eta-function with explicit infinitesimal im parts        *
!     pz = z1*z2                                                       *
!----------------------------------------------------------------------*
!     06.10.08    Ansgar Denner       last changed   06.10.08          *
!***********************************************************************
      implicit none
      complex(kind=prec) :: eta2s_coli,z1,z2,pz
      real(kind=prec)    :: i1,i2,ip
      real(kind=prec)    :: im1,im2,imp,re1,re2,rep
      logical :: errorwriteflag

      pz=z1*z2

      im1 = aimag(z1)
      im2 = aimag(z2)
      imp = aimag(pz)
      re1 = real(z1)
      re2 = real(z2)
      rep = real(pz)


      if (abs(im1).lt.impacc*abs(re1)) im1 = rnul
      if (abs(im2).lt.impacc*abs(re2)) im2 = rnul
      if (abs(imp).lt.impacc*abs(rep)) imp = rnul


      if (im1.ne.rnul.or.im2.ne.rnul.or.imp.ne.rnul) then
        if(im1.eq.rnul) im1 = i1
        if(im2.eq.rnul) im2 = i2
        if(imp.eq.rnul) imp = ip
        if(im1.lt.rnul.and.im2.lt.rnul.and.imp.gt.rnul) then
          eta2s_coli = rtwo*pi*cima
        else if (im1.gt.rnul.and.im2.gt.rnul.and.imp.lt.rnul) then
          eta2s_coli = -rtwo*pi*cima
        else
          eta2s_coli = cnul
        endif
      else
        eta2s_coli = cnul
        if (re1.lt.rnul.and.re2.lt.rnul) then
          if(i1.gt.rnul.and.i2.gt.rnul) then
            eta2s_coli = -rtwo*pi*cima
          else   if(i1.lt.rnul.and.i2.lt.rnul) then
            eta2s_coli = rtwo*pi*cima
          endif
        else if (rep.lt.rnul.and.re1.lt.rnul) then
          if(i1.gt.rnul.and.ip.lt.rnul) then
            eta2s_coli = -rtwo*pi*cima
          else   if(i1.lt.rnul.and.ip.gt.rnul) then
            eta2s_coli = rtwo*pi*cima
          endif
        else if (rep.lt.rnul.and.re2.lt.rnul) then
          if(i2.gt.rnul.and.ip.lt.rnul) then
            eta2s_coli = -rtwo*pi*cima
          else   if(i2.lt.rnul.and.ip.gt.rnul) then
            eta2s_coli = rtwo*pi*cima
          endif
        endif
      endif


      end

!***********************************************************************
      function chdet(n,cai)
!***********************************************************************
!     Calculation of the determinant of a complex n x n matrix         *
!     The matrix is first reduced via Householder transformations into *
!     the form A = QR                                                  *
!     where  Q is a orthogonal and R a upper triangular matrix.        *
!     See Stoer, Numerische Mathematik, chapter 4.7                    *
!     The determinant is then obtained as                              *
!     Det(a) = prod_i (-r_ii)                                          *
!     ca(i,j) j.ge.i contains the elements of u_i that form the        *
!             Householder matrix P_i = 1 - beta_i u x u^H              *
!     ca(i,j) j.lt.i contains the nondiagonal entries of R             *
!     cd contains the diagonal elements of R                           *
!----------------------------------------------------------------------*
!     20.04.04  Ansgar Denner     last changed  08.06.04               *
!***********************************************************************
      implicit none
      integer :: n
      complex(kind=prec) :: chdet
      real(kind=prec)    :: aabs,sigma,rs,beta,norm
      complex(kind=prec) :: ca(n,n),cd(n),cq(n,n),cr(n,n),catest(n,n)
      complex(kind=prec) :: cai(n,n)
      complex(kind=prec) :: cdet,csum,cs
      integer :: i,j,k
      logical :: errorwriteflag

! saving of input

      do 10 j=1,n
        do 20 i=1,n
          ca(i,j) = cai(i,j)
 20     continue
 10   continue


!  Householder transformation

      do 100 j=1,n

!  calculation of transformation matrix

        aabs = abs(ca(j,j))
        sigma = aabs**2
        do 200 i=j+1,n
           sigma = sigma +  abs(ca(i,j))**2
 200    continue
        if (sigma.eq.0) then
          chdet = rnul
          return
        end if
        rs = sqrt(sigma)
        if (aabs.gt.rnul) then
          cs = rs* ca(j,j)/aabs
        else
          cs = rs
        end if
        beta = rone/(sigma + rs * aabs)
        ca(j,j) = ca(j,j) + cs

! multiplication of a(i,j) with the transformation matrix

        cd(j) = -cs
        do 300 k=j+1,n
          csum = rnul
          do 400 i=j,n
            csum = csum + conjg(ca(i,j))*ca(i,k)
 400      continue
          csum = csum*beta
          do 500 i=j,n
            ca(i,k) = ca(i,k) - ca(i,j) * csum
 500      continue
 300    continue

 100  continue


! calculation of determinant
      cdet = rone
      do 600 i=1,n
        cdet = -cdet * cd(i)
 600  continue
      chdet = cdet


      return

! calculation of matrix Q

      do 700 j=n,1,-1
        norm = rnul
        do 800 i=j,n
          norm = norm + abs(ca(i,j))**2
 800    continue
        cq(j,j) = 1 - rtwo * abs(ca(j,j))**2/norm
        do 900 i=j+1,n
          cq(i,j) = - rtwo * ca(i,j)*conjg(ca(j,j))/norm
 900    continue
        do 1000 k=j+1,n
          csum = rnul
          do 1100 i=j+1,n
            csum = csum + conjg(ca(i,j))*cq(i,k)
 1100     continue
          csum = rtwo*csum/norm
          cq(j,k) =  - ca(j,j) * csum
          do 1200 i=j+1,n
            cq(i,k) = cq(i,k) - ca(i,j) * csum
 1200     continue
 1000   continue
 700  continue

! determination of matrix R

      do i=1,n
      do k=1,n
        cr(i,k) = rnul
      end do
      end do
      do i=1,n
        cr(i,i) = cd(i)
      do k=i+1,n
        cr(i,k) = ca(i,k)
      end do
      end do

! check of unitarity of Q

      do i=1,n
      do k=1,n
        catest(i,k) = rnul
      do j=1,n
        catest(i,k) = catest(i,k) + cq(i,j)*conjg(cq(k,j))
      end do
      end do
      end do

! check of decomposition
      do i=1,n
      do k=1,n
        catest(i,k) = rnul
      do j=1,k
        catest(i,k) = catest(i,k) + cq(i,j)*cr(j,k)
      end do
      end do
      end do

      end

!!***********************************************************************
!      subroutine chinve(n,cai,cainv,cdeta,cdetaaccloss)
!!***********************************************************************
!!     Calculation of the inverse of a complex n x n matrix             *
!!     The matrix is first reduced via Householder transformations into *
!!     the form A = QR                                                  *
!!     where  Q is a hermitean and R a upper triangular matrix.         *
!!     See Stoer, Numerische Mathematik, chapter 4.7                    *
!!     The inverse is then obtained as                                  *
!!     Ainv = Rinv * Qadj                                               *
!!     ca(i,j) j.ge.i contains the elements of u_i that form the        *
!!             Householder matrix P_i = 1 - beta_i u x u^H              *
!!     ca(i,j) j.lt.i contains the nondiagonal entries of R             *
!!     cd contains the diagonal elements of R                           *
!!     flag = 1 signals vanishing determinant                           *
!!     cdetaaccloss:  error in cdeta propto  1/cdetaaccloss             *
!!       log(cdetaaccloss)/log(10*rone) = number of lost digits of cdeta   *
!!----------------------------------------------------------------------*
!!     07.06.05  Ansgar Denner     last changed  24.01.19               *
!!***********************************************************************
!      implicit none
!      complex(kind=prec) ::, intent(out) :: cdeta
!      real(kind=prec) ::,    intent(out) :: cdetaaccloss
!      integer    n
!      complex(kind=prec) :: chdet
!      real(kind=prec) ::     aabs,sigma,rs,beta,norm
!      complex(kind=prec) :: ca(n,n),cd(n),cq(n,n),cr(n,n),catest(n,n),crtest(n,n)
!      complex(kind=prec) :: cai(n,n),crinv(n,n),cainv(n,n)
!      complex(kind=prec) :: csum,cs,canew
!      real(kind=prec) ::     caaccloss(n),cdaccloss(n),csaccloss
!      integer    i,j,k
!      logical    errorwriteflag
!
!! saving of input
!      do 10 j=1,n
!        do 20 i=1,n
!          ca(i,j) = cai(i,j)
! 20     continue
! 10   continue
!
!!      if(n.eq.4) then
!!      write(*,2) cai(1,1:4)
!!      write(*,2) cai(2,1:4)
!!      write(*,2) cai(3,1:4)
!!      write(*,2) cai(4,1:4)
!!      end if
!
! 2    format(5('(',g21.14,',',g21.14,') ':))
!
!!  Householder transformation
!
!      do 100 j=1,n
!
!!  calculation of transformation matrix
!
!        aabs = abs(ca(j,j))
!        sigma = aabs**2
!        do 200 i=j+1,n
!           sigma = sigma +  abs(ca(i,j))**2
! 200    continue
!        if (sigma.eq.0) then
!          cdeta = rnul
!          return
!        end if
!        rs = sqrt(sigma)
!        if (aabs.gt.rnul) then
!          cs = rs* (ca(j,j)/aabs)
!#ifdef ERREST
!!          csaccloss = caaccloss(j)
!!  propagation of error of ca(j,j)
!          if (imag(ca(j,j))*real(ca(j,j)).ne.rnul) then
!            csaccloss = min(rone, caaccloss(j)*sigma/aabs**2,
!     &          caaccloss(j)*aabs**2/abs(imag(ca(j,j))*real(ca(j,j))))
!          else
!            csaccloss = min(rone,caaccloss(j)*sigma/aabs**2)
!          end if
!#endif
!        else
!          cs = rs
!#ifdef ERREST
!          csaccloss = rone
!#endif
!        end if
!
!        beta = rone/(sigma + rs * aabs)
!        ca(j,j) = ca(j,j) + cs
!
!! multiplication of a(i,j) with the transformation matrix
!
!        cd(j) = -cs
!#ifdef ERREST
!        cdaccloss(j) = csaccloss
!#endif
!        do 300 k=j+1,n
!          csum = rnul
!          do 400 i=j,n
!            csum = csum + conjg(ca(i,j))*ca(i,k)
! 400      continue
!          csum = csum*beta
!          do 500 i=j,n
!            canew = ca(i,k) - ca(i,j) * csum
!#ifdef ERREST
!!  estimate error of ca(k,k)
!            if (i.eq.k) then
!              if(ca(i,k).ne.rnul) then
!                if(ca(i,j)*csum.ne.rnul) then
!                  caaccloss(i) = min(rone,
!     &                caaccloss(i)* abs(canew/abs(ca(i,k))),
!     &                abs(canew/abs(ca(i,j)*csum)))
!                else
!                  caaccloss(i) = min(rone,
!     &                caaccloss(i)* abs(canew/abs(ca(i,k))))
!                endif
!              else
!                caaccloss(i) = rone
!              endif
!            end if
!#endif
!            ca(i,k) = canew
! 500      continue
! 300    continue
!
! 100  continue
!
!!      write(*,*) 'chinve cdaccloss = ',cdaccloss
!
!! calculation of determinant
!      cdeta = rone
!#ifdef ERREST
!      cdetaaccloss = rone
!#endif
!      do 600 i=1,n
!        cdeta = -cdeta * cd(i)
!#ifdef ERREST
!        cdetaaccloss = cdetaaccloss * cdaccloss(i)
!#endif
! 600  continue
!
!
!! calculation of matrix Q
!
!      do 700 j=n,1,-1
!        norm = rnul
!        do 800 i=j,n
!          norm = norm + abs(ca(i,j))**2
! 800    continue
!        cq(j,j) = 1 - rtwo * abs(ca(j,j))**2/norm
!        do 900 i=j+1,n
!          cq(i,j) = - rtwo * ca(i,j)*conjg(ca(j,j))/norm
! 900    continue
!        do 1000 k=j+1,n
!          csum = rnul
!          do 1100 i=j+1,n
!            csum = csum + conjg(ca(i,j))*cq(i,k)
! 1100     continue
!          csum = rtwo*csum/norm
!          cq(j,k) =  - ca(j,j) * csum
!          do 1200 i=j+1,n
!            cq(i,k) = cq(i,k) - ca(i,j) * csum
! 1200     continue
! 1000   continue
! 700  continue
!
!! determination of matrix R
!
!      do i=1,n
!      do k=1,n
!        cr(i,k) = rnul
!      end do
!      end do
!      do i=1,n
!        cr(i,i) = cd(i)
!      do k=i+1,n
!        cr(i,k) = ca(i,k)
!      end do
!      end do
!
!! determination of inverse of R
!
!      do i=1,n
!      do k=1,n
!        crinv(i,k) = rnul
!      end do
!      end do
!      do k=1,n
!        crinv(k,k) = rone/cr(k,k)
!      do i=1,k-1
!      do 2200 j=i,k-1
!        crinv(i,k) = crinv(i,k) - crinv(i,j)* cr(j,k)/cr(k,k)
! 2200 continue
!      end do
!      end do
!
!! determination of inverse of A
!
!      do i=1,n
!      do k=1,n
!        cainv(i,k) = rnul
!      do j=1,n
!        cainv(i,k) = cainv(i,k) + crinv(i,j)*conjg(cq(k,j))
!      end do
!      end do
!      end do
!
!      return
!
!! check of unitarity of Q
!
!      do i=1,n
!      do k=1,n
!        catest(i,k) = rnul
!      do j=1,n
!        catest(i,k) = catest(i,k) + cq(i,j)*conjg(cq(k,j))
!      end do
!      end do
!      end do
!
!
!! check of decomposition
!
!      do i=1,n
!      do k=1,n
!        catest(i,k) = rnul
!      do j=1,k
!        catest(i,k) = catest(i,k) + cq(i,j)*cr(j,k)
!      end do
!      end do
!      end do
!
!
!! check of inverse of Q
!      do i=1,n
!      do k=1,n
!        crtest(i,k) = rnul
!      do j=1,n
!        crtest(i,k) = crtest(i,k) + cr(i,j)*crinv(j,k)
!      end do
!      end do
!      end do
!
!! check of inverse of A
!
!      do i=1,n
!      do k=1,n
!        catest(i,k) = rnul
!      do j=1,n
!        catest(i,k) = catest(i,k) + cai(i,j)*cainv(j,k)
!      end do
!      end do
!      end do
!
!      end
!
!************************************************************************
!      subroutine chinv(n,cai,cainv,cdeta)
!************************************************************************
!*     Calculation of the inverse of a complex n x n matrix             *
!*     The matrix is first reduced via Householder transformations into *
!*     the form A = QR                                                  *
!*     where  Q is a hermitean and R a upper triangular matrix.         *
!*     See Stoer, Numerische Mathematik, chapter 4.7                    *
!*     The inverse is then obtained as                                  *
!*     Ainv = Rinv * Qadj                                               *
!*     ca(i,j) j.ge.i contains the elements of u_i that form the        *
!*             Householder matrix P_i = 1 - beta_i u x u^H              *
!*     ca(i,j) j.lt.i contains the nondiagonal entries of R             *
!*     cd contains the diagonal elements of R                           *
!*     flag = 1 signals vanishing determinant                           *
!*----------------------------------------------------------------------*
!*     07.06.05  Ansgar Denner     last changed  21.06.18               *
!************************************************************************
!      implicit none
!      complex(kind=prec) ::, intent(out), optional :: cdeta
!      integer    n
!      complex(kind=prec) :: chdet
!      real(kind=prec) ::     aabs,sigma,rs,beta,norm
!      complex(kind=prec) :: ca(n,n),cd(n),cq(n,n),cr(n,n),catest(n,n),crtest(n,n)
!      complex(kind=prec) :: cai(n,n),crinv(n,n),cainv(n,n)
!      complex(kind=prec) :: csum,cs
!      integer    i,j,k
!      logical    errorwriteflag
!
!c saving of input
!      do 10 j=1,n
!        do 20 i=1,n
!          ca(i,j) = cai(i,j)
! 20     continue
! 10   continue
!
! 2    format(5('(',g11.4,',',g11.4,') ':))
!
!c  Householder transformation
!
!      do 100 j=1,n
!
!c  calculation of transformation matrix
!
!        aabs = abs(ca(j,j))
!        sigma = aabs**2
!        do 200 i=j+1,n
!           sigma = sigma +  abs(ca(i,j))**2
! 200    continue
!        if (sigma.eq.0) then
!          chdet = rnul
!#ifdef NONE
!!         det=0 taken care of by reduction subroutines!
!          call setErrFlag_coli(-7)
!          call ErrOut_coli('chinv',' zero determinant',
!     &       errorwriteflag)
!          if (errorwriteflag) then
!            write(nerrout_coli,*) 'sigma = chdet =',chdet
!            write(nerrout_coli,*) 'n = ',n
!          endif
!#endif
!          if(present(cdeta)) cdeta = chdet
!          return
!        end if
!        rs = sqrt(sigma)
!        if (aabs.gt.rnul) then
!          cs = rs* ca(j,j)/aabs
!        else
!          cs = rs
!        end if
!        beta = rone/(sigma + rs * aabs)
!        ca(j,j) = ca(j,j) + cs
!
!c multiplication of a(i,j) with the transformation matrix
!
!        cd(j) = -cs
!        do 300 k=j+1,n
!          csum = rnul
!          do 400 i=j,n
!            csum = csum + conjg(ca(i,j))*ca(i,k)
! 400      continue
!          csum = csum*beta
!          do 500 i=j,n
!            ca(i,k) = ca(i,k) - ca(i,j) * csum
! 500      continue
! 300    continue
!
! 100  continue
!
!
!c calculation of determinant
!      cdeta = rone
!      do 600 i=1,n
!        cdeta = -cdeta * cd(i)
! 600  continue
!
!
!c calculation of matrix Q
!
!      do 700 j=n,1,-1
!        norm = rnul
!        do 800 i=j,n
!          norm = norm + abs(ca(i,j))**2
! 800    continue
!        cq(j,j) = 1 - rtwo * abs(ca(j,j))**2/norm
!        do 900 i=j+1,n
!          cq(i,j) = - rtwo * ca(i,j)*conjg(ca(j,j))/norm
! 900    continue
!        do 1000 k=j+1,n
!          csum = rnul
!          do 1100 i=j+1,n
!            csum = csum + conjg(ca(i,j))*cq(i,k)
! 1100     continue
!          csum = rtwo*csum/norm
!          cq(j,k) =  - ca(j,j) * csum
!          do 1200 i=j+1,n
!            cq(i,k) = cq(i,k) - ca(i,j) * csum
! 1200     continue
! 1000   continue
! 700  continue
!
!c determination of matrix R
!
!      do i=1,n
!      do k=1,n
!        cr(i,k) = rnul
!      end do
!      end do
!      do i=1,n
!        cr(i,i) = cd(i)
!      do k=i+1,n
!        cr(i,k) = ca(i,k)
!      end do
!      end do
!
!c determination of inverse of R
!
!      do i=1,n
!      do k=1,n
!        crinv(i,k) = rnul
!      end do
!      end do
!      do k=1,n
!        crinv(k,k) = rone/cr(k,k)
!      do i=1,k-1
!      do 2200 j=i,k-1
!        crinv(i,k) = crinv(i,k) - crinv(i,j)* cr(j,k)/cr(k,k)
! 2200 continue
!      end do
!      end do
!
!c determination of inverse of A
!
!      do i=1,n
!      do k=1,n
!        cainv(i,k) = rnul
!      do j=1,n
!        cainv(i,k) = cainv(i,k) + crinv(i,j)*conjg(cq(k,j))
!      end do
!      end do
!      end do
!
!      return
!
!c check of unitarity of Q
!
!      do i=1,n
!      do k=1,n
!        catest(i,k) = rnul
!      do j=1,n
!        catest(i,k) = catest(i,k) + cq(i,j)*conjg(cq(k,j))
!      end do
!      end do
!      end do
!
!
!c check of decomposition
!
!      do i=1,n
!      do k=1,n
!        catest(i,k) = rnul
!      do j=1,k
!        catest(i,k) = catest(i,k) + cq(i,j)*cr(j,k)
!      end do
!      end do
!      end do
!
!c check of inverse of Q
!      do i=1,n
!      do k=1,n
!        crtest(i,k) = rnul
!      do j=1,n
!        crtest(i,k) = crtest(i,k) + cr(i,j)*crinv(j,k)
!      end do
!      end do
!      end do
!
!c check of inverse of A
!
!      do i=1,n
!      do k=1,n
!        catest(i,k) = rnul
!      do j=1,n
!        catest(i,k) = catest(i,k) + cai(i,j)*cainv(j,k)
!      end do
!      end do
!      end do
!
!      end


!***********************************************************************
  subroutine setmuuv2_coli(muuv2in)
!***********************************************************************
!     sets UV-regulator mass squared to muuv2in                        *
!-----------------------------------------------------------------------
!     28.08.08 Ansgar Denner         last changed  28.08.08            *
!***********************************************************************
  implicit none
  real(kind=prec) :: muuv2in

  muuv2 = muuv2in

  if(coliinfo)then
    write(ninfout_coli,*) &
       'COLI: UV regulator mass squared set to muuv2 = ', &
       muuv2
  endif

  end
!***********************************************************************
  subroutine getmuuv2_coli(muuv2out)
!***********************************************************************
!     returns UV-regulator mass squared to muuv2in                     *
!-----------------------------------------------------------------------
!     23.10.08 Ansgar Denner         last changed  23.10.08            *
!***********************************************************************
  implicit none
  real(kind=prec) :: muuv2out

  muuv2out = muuv2

  end

!***********************************************************************
  subroutine setmuir2_coli(muir2in)
!***********************************************************************
!     sets photon mass squared regulator to lambda2in                  *
!-----------------------------------------------------------------------
!     28.08.08 Ansgar Denner         last changed  28.08.08            *
!***********************************************************************
  implicit none
  real(kind=prec) :: muir2in

  muir2 = muir2in

  if(coliinfo)then
    write(ninfout_coli,*) &
        'COLI: IR regularor mass squared set to muir2 = ', &
        muir2
  endif

  end
!***********************************************************************
  subroutine getmuir2_coli(muir2out)
!***********************************************************************
!     returns UV-regulator mass squared to muir2in                     *
!-----------------------------------------------------------------------
!     23.10.08 Ansgar Denner         last changed  23.10.08            *
!***********************************************************************
  implicit none
  real(kind=prec) :: muir2out

  muir2out = muir2

  end


#ifdef SING
!***********************************************************************
  subroutine setdeltauv_coli(duv)
!***********************************************************************
!     sets UV pole 2/(4-D) to deltauv                                  *
!-----------------------------------------------------------------------
!     28.08.08 Ansgar Denner         last changed  28.08.08            *
!***********************************************************************
  implicit none
  real(kind=prec) :: duv

  deltauv = duv
  if(coliinfo)then
    write(ninfout_coli,*) &
        'COLI: UV pole set to                 deltauv = ', &
      deltauv
  endif

  end
!***********************************************************************
  subroutine getdeltauv_coli(duv)
!***********************************************************************
!     returns UV pole 2/(4-D)  deltauv                                 *
!-----------------------------------------------------------------------
!     23.10.08 Ansgar Denner         last changed  23.10.08            *
!***********************************************************************
  implicit none
  real(kind=prec) :: duv

  duv = deltauv

  end
!***********************************************************************
  subroutine setdeltair_coli(dir1,dir2)
!***********************************************************************
!     sets IR pole 2/(4-D) to delta1ir and squared pole to delta2ir    *
!-----------------------------------------------------------------------
!     28.08.08 Ansgar Denner         last changed  28.08.08            *
!***********************************************************************
  implicit none
  real(kind=prec) :: dir1,dir2

  delta1ir = dir1
  delta2ir = dir2
  if(coliinfo)then
    write(ninfout_coli,*) &
       'COLI: IR pole set to                delta1ir = ', &
       delta1ir
    write(ninfout_coli,*) &
       'COLI: squared IR pole set to        delta2ir = ', &
       delta2ir
  endif
  end
!***********************************************************************
  subroutine getdeltair_coli(dir1,dir2)
!***********************************************************************
!     returns UV pole 2/(4-D)  deltauv                                 *
!-----------------------------------------------------------------------
!     23.10.08 Ansgar Denner         last changed  23.10.08            *
!***********************************************************************
  implicit none
  real(kind=prec) :: dir1,dir2

  dir1 = delta1ir
  dir2 = delta2ir

  end
!***********************************************************************
  subroutine setshiftms2_coli(shiftms2in)
!***********************************************************************
!     sets  shift for squared mass-singular logs to shiftms2in         *
!-----------------------------------------------------------------------
!     28.08.08 Ansgar Denner         last changed  28.08.08            *
!***********************************************************************
  implicit none
  real(kind=prec) :: shiftms2in

  colishiftms2 = shiftms2in

  if(coliinfo)then
    write(ninfout_coli,*) &
       'COLI: shift for squared mass-singular logs set to'
    write(ninfout_coli,*) &
       '                               colishiftms2  = ', &
       colishiftms2
  endif

  end

!***********************************************************************
  subroutine getshiftms2_coli(shiftms2out)
!***********************************************************************
!     gets shift for squared mass-singular logs colishiftms2           *
!-----------------------------------------------------------------------
!     15.09.08 Ansgar Denner         last changed  15.09.08            *
!***********************************************************************
  implicit none
  real(kind=prec) :: shiftms2out

  shiftms2out=colishiftms2

  end
#endif

!***********************************************************************
  subroutine setminf2_coli(m2)
!***********************************************************************
!     adds m2 to list of mass squared to be neglected                  *
!-----------------------------------------------------------------------
!     28.08.08 Ansgar Denner         last changed  28.08.08            *
!***********************************************************************
  implicit none
  complex(kind=prec) :: m2
  integer :: i

  do 10 i=1,ncoliminf
    if(m2 .eq. coliminffix2(i)) return
10   continue

  ncoliminf = ncoliminf+1
  coliminffix(ncoliminf) = sqrt(m2)
  coliminffix2(ncoliminf) = m2
  coliminf2(ncoliminf) = m2/coliminfscale2
  coliminf(ncoliminf) = sqrt(coliminf2(ncoliminf))

  if(coliinfo)then
    write(ninfout_coli,*) &
       'COLI: added to small masses squared:  m2 =  ',m2
  endif

  end
!***********************************************************************
  subroutine clearcoliminf2()
!***********************************************************************
!     clears list of masses squared to be neglected                    *
!-----------------------------------------------------------------------
!     28.08.08 Ansgar Denner         last changed  28.08.08            *
!***********************************************************************
  implicit none
  integer :: i

  do 10 i=1,ncoliminf
    coliminffix2(i) = rnul
    coliminffix(i) = rnul
    coliminf2(i) = rnul
    coliminf(i) = rnul
10   continue

  ncoliminf = 0

  if(coliinfo)then
    write(ninfout_coli,*) &
       'COLI: list of complex small masses cleared'
  endif

  end

!***********************************************************************
  subroutine setminfscale2_coli(scale2)
!***********************************************************************
!     sets factor with which small masses are scaled up in             *
!     mass-singular logarithms                                         *
!     and rescales masses in /coliminf/ /coliminf2/                    *
!-----------------------------------------------------------------------
!     28.08.08 Ansgar Denner         last changed  28.08.08            *
!***********************************************************************
  implicit none
  real(kind=prec) :: scale2
  integer :: i
  coliminfscale2 = scale2
  coliminfscale = sqrt(scale2)

  do 10 i=1,ncoliminf
    coliminf(i) = coliminffix(i)/coliminfscale
    coliminf2(i) = coliminffix2(i)/coliminfscale2
10   continue

  if(coliinfo)then
    write(ninfout_coli,*) &
       'COLI: small masses scaled with coliminfscale = ', &
       coliminfscale
    write(ninfout_coli,*) '      in mass-singular logarithms'
  endif

  end

!***********************************************************************
  subroutine modminfscale2_coli(scale2)
!***********************************************************************
!     sets factor with which small masses are scaled up in             *
!     mass-singular logarithms                                         *
!-----------------------------------------------------------------------
!     15.09.09 Ansgar Denner         last changed  15.09.09            *
!***********************************************************************
  implicit none
  real(kind=prec) :: scale2

  coliminfscale2 = scale2
  coliminfscale = sqrt(scale2)
  if(coliinfo)then
    write(ninfout_coli,*) 'COLI: scalefactor for small masses ', &
       ' in mass-singular logarithms'
    write(ninfout_coli,*) &
       '     changed to coliminfscale = ', &
       coliminfscale
  endif

  end

!***********************************************************************
  subroutine getminfscale2_coli(scale2)
!***********************************************************************
!     gets factor with which small masses are scaled up in             *
!     mass-singular logarithms                                         *
!-----------------------------------------------------------------------
!     15.09.09 Ansgar Denner         last changed  15.09.09            *
!***********************************************************************
  implicit none
  real(kind=prec) :: scale2

  scale2=coliminfscale2
  end

!***********************************************************************
  function minfscaledown_coli(m)
!***********************************************************************
!     scales mass down by coliminfscale if m in /coliminf/             *
!-----------------------------------------------------------------------
!     28.08.08 Ansgar Denner         last changed  28.08.08            *
!***********************************************************************
  implicit none
  complex(kind=prec) :: minfscaledown_coli,m
  integer :: i

  minfscaledown_coli = m
  do 10 i=1,ncoliminf
     if(m.eq.coliminffix(i)) then
       minfscaledown_coli = coliminf(i)
       return
     endif
10   continue

  end

!***********************************************************************
  function minfscaledown2_coli(m2)
!***********************************************************************
!     scales mass down by coliminfscale^2 if m2 in /coliminf2/         *
!-----------------------------------------------------------------------
!     28.08.08 Ansgar Denner         last changed  28.08.08            *
!***********************************************************************
  implicit none

  complex(kind=prec) :: minfscaledown2_coli,m2
  integer :: i

  minfscaledown2_coli = m2
  do 10 i=1,ncoliminf
     if(m2 .eq. coliminffix2(i)) then
       minfscaledown2_coli = coliminf2(i)
       return
     endif
10   continue

  end

!***********************************************************************
  function elimminf_coli(m)
!***********************************************************************
!     if m = coliminfs(i)  then  elimcoliminf = 0                      *
!                          else  elimcoliminf  = m                     *
!-----------------------------------------------------------------------
!     28.08.08 Ansgar Denner         last changed  08.09.08            *
!***********************************************************************
  implicit none
  complex(kind=prec) :: m,elimminf_coli
  integer :: i

  elimminf_coli = m
  do 10 i=1,ncoliminf
    write(*,*) "coliminf(i):", coliminf(i)
     if(m.eq.coliminf(i)) then
       elimminf_coli = rnul
       return
     endif
10   continue
  end

!***********************************************************************
  function elimminf2_coli(m2)
!***********************************************************************
!     if m2 = coliminf2(i)  then  elimminf2_coli = 0                   *
!                           else  elimminf2_coli = m2                  *
!-----------------------------------------------------------------------
!     28.08.08 Ansgar Denner         last changed  28.08.08            *
!***********************************************************************
  implicit none
  complex(kind=prec) :: m2,elimminf2_coli
  integer :: i

  elimminf2_coli = m2
  do 10 i=1,ncoliminf
     if(m2 .eq. coliminf2(i)) then
       elimminf2_coli = rnul
       return
     endif
10   continue

  end

!***********************************************************************
  subroutine setirratterms_coli
!***********************************************************************
!     sets flag to include IR rational terms                           *
!     in derivatives of 2-point functions                              *
!-----------------------------------------------------------------------
!     27.03.15 Ansgar Denner         last changed  27.03.15            *
!***********************************************************************
  implicit none

  ir_rat_terms=.true.
  if(coliinfo)then
    write(ninfout_coli,*) 'COLI: IR rational terms included'
  end if

  end

!***********************************************************************
  subroutine unsetirratterms_coli
!***********************************************************************
!     unsets flag to include IR rational terms                         *
!     in derivatives of 2-point functions                              *
!-----------------------------------------------------------------------
!     27.03.15 Ansgar Denner         last changed  27.03.15            *
!***********************************************************************
  implicit none

  ir_rat_terms=.false.
  if(coliinfo)then
     write(ninfout_coli,*) 'COLI: IR rational terms not included'
  endif

  end

!***********************************************************************
  subroutine setinfo_coli
!***********************************************************************
!     sets photon mass squared regulator to lambda2in                  *
!-----------------------------------------------------------------------
!     15.09.09 Ansgar Denner         last changed  15.09.09            *
!***********************************************************************
  implicit none
  coliinfo=.true.
  if(ninfout_coli.ne.6.and.ninfout_coli.ne.0) then
    write(*,*) 'COLI: information printed'
  end if
  write(ninfout_coli,*) 'COLI: information printed'

  end
!***********************************************************************
  subroutine unsetinfo_coli
!***********************************************************************
!     sets photon mass squared regulator to lambda2in                  *
!-----------------------------------------------------------------------
!     15.09.09 Ansgar Denner         last changed  15.09.09            *
!***********************************************************************
  implicit none

  coliinfo=.false.

  end


  subroutine ErrOut_coli(sub,err,flag)

    character(len=*), intent(in) :: sub, err
    logical, intent(out) :: flag

    flag = .false.
    if(erroutlev_coli.eq.0) return

    ErrCnt_coli = ErrCnt_coli + 1
    if (ErrCnt_coli.le.maxErrOut_coli) then
      write(nerrout_coli,*)
      write(nerrout_coli,*)
      write(nerrout_coli,*)
      write(nerrout_coli,*) '***********************************************************'
      write(nerrout_coli,*) 'ERROR NO.', ErrCnt_coli
      write(nerrout_coli,*) 'in routine: ', trim(sub)
      write(nerrout_coli,*) trim(err)
      flag = .true.
    elseif (ErrCnt_coli.eq.maxErrOut_coli+1) then
      write(nerrout_coli,*)
      write(nerrout_coli,*)
      write(nerrout_coli,*)
      write(nerrout_coli,*) '***********************************************************'
      write(nerrout_coli,*)
      write(nerrout_coli,*) ' Further output of Errors will be suppressed '
      write(nerrout_coli,*)
    endif

  end subroutine ErrOut_coli

  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !  subroutine SetErrFlag_coli(err)
  !
  !  -1 Check failed
  !  -4 Argument on cut       (only if CHECK set)
  !  -5 Critical event
  !  -6 No reduction method works
  !  -6 momenta not 4-dimensional
  !  -7 specific numerical problem
  !
  ! -10 Case not supported/implemented
  !
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  subroutine SetErrFlag_coli(err)

    integer, intent(in) :: err

    ErrFlag_coli = err

  end subroutine SetErrFlag_coli

end module
