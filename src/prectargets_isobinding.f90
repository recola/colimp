#import "prectargets_isobinding.inc"

module prectargets_isobinding_<T>
  use, intrinsic :: ISO_C_Binding, only: floating_type => @T
  implicit none
end module prectargets_isobinding_<T>
