!**********************************************************************
!     file params_coli.h                                              *
!     global input parameters for library COLI                        *
!---------------------------------------------------------------------*
!     02.05.08  Ansgar Denner     last changed  27.03.15              *
!**********************************************************************
#import "prectemplate.inc"

module params_coli_<T>
  use prectargets_<T>
  implicit none
  integer, parameter :: prec = <T>
  real(kind=prec), parameter :: &
pi=3.1415926535897932384626433832795028841971693993751058209749445923078_prec, &
pi2=9.8696044010893586188344909998761511353136994072407906264133493762200_prec,&
pi2_6=1.6449340668482264364724151666460251892189499012067984377355582293700_prec
  real(kind=prec), parameter :: &
      rnul=0._prec, &
      rone=1._prec, &
      rtwo=2._prec, &
      rhlf=1/2._prec
  complex(kind=prec) :: &
    cnul=(0._prec,0._prec), &
    cone=(1._prec,0._prec), &
    cima=(0._prec,1._prec), &
    undefined=(huge(1._prec))
  integer, parameter :: nerrout_coli=81, &
                        ninfout_coli=6, &
                        MaxErrOut_coli=100
  integer :: ErrCnt_coli,erroutlev_coli,ErrFlag_coli

end module params_coli_<T>
