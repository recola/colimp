typedef float real_sp;
typedef coliflower::CType<real_sp> cmplx_sp;

typedef double real_dp;
typedef coliflower::CType<real_dp> cmplx_dp;

typedef long double real_lp;
typedef coliflower::CType<real_lp> cmplx_lp;

typedef __float128  real_qp;
typedef coliflower::CType<real_qp> cmplx_qp;
