!!
!!  File coli_d0reg.F is part of COLLIER
!!  - A Complex One-Loop Library In Extended Regularizations
!!
!!  Copyright (C) 2015, 2016   Ansgar Denner, Stefan Dittmaier, Lars Hofer
!!
!!  COLLIER is licenced under the GNU GPL version 3, see COPYING for details.
!!
#import "prectemplate.inc"

module coli_d0reg_<T>
  use params_coli_<T>
  use common_coli_<T>
  use checkparams_coli
  use coli_aux_<T>, only: elimminf2_coli=>elimminf2_coli, chdet, &
    cln_coli, eta2_coli, eta2s_coli, cspenc_coli, cspcon_coli, cspcos_coli
  implicit none

  contains
!***********************************************************************
!                                                                      *
!     Regular scalar 4-point function                                  *
!                                                                      *
!***********************************************************************
!                                                                      *
!     last changed  15.06.11  Ansgar Denner                            *
!     errorflags    24.05.13  Ansgar Denner   updated 26.03.15         *
!                                                                      *
!***********************************************************************
! Subroutines:                                                         *
! Functions:                                                           *
! D0regrp_coli,D0comb_coli                                             *
!***********************************************************************
  function D0regrp_coli(p12,p23,p34,p14,p13,p24,m12,m22,m32,m42)
!***********************************************************************
!  scalar 4-point function  for r13 real positive                      *
!  regular case  based on general result of                            *
!        A.Denner, U.Nierste and R.Scharf, Nucl. Phys. B367 (1991) 637 *
!  valid for real positive r13 and vicinity                            *
!                                                                      *
!                     m22                                              *
!       p12  ---------------------  q23                                *
!                 |    2    |                                          *
!                 |         |                                          *
!              m12| 1     3 | m32                                      *
!                 |         |                                          *
!                 |    4    |                                          *
!       q14  ---------------------  q34                                *
!                     m42                                              *
!                                                                      *
!----------------------------------------------------------------------*
!  29.12.08 Ansgar Denner       last changed 17.03.10 Ansgar Denner    *
!***********************************************************************
  implicit none
  complex(kind=prec) :: D0regrp_coli
  complex(kind=prec) :: p12,p23,p34,p14,p13,p24
  complex(kind=prec) :: m12,m22,m32,m42

  complex(kind=prec) :: l12,l13,l14,l23,l24,l34
  complex(kind=prec) :: r12,r13,r14,r23,r24,r34
  complex(kind=prec) :: r21,r31,r41,r32,r42,r43
  complex(kind=prec) :: a,b,c,d,det
  complex(kind=prec) :: x(2,4),yc(4,4)
  complex(kind=prec) :: ch0,ch1,ch2,ch3,ch4,ch5,l1,l2,argl1
  complex(kind=prec) :: eta
  real(kind=prec) ::     ir12,ir13,ir14,ir23,ir24,ir34
  real(kind=prec) ::     ix(2,4),ipop(2)
  real(kind=prec) ::     test0,test1,test2,test3,test4,test5
  real(kind=prec) ::     test01,test23,test45
  real(kind=prec) ::     u,v
  integer :: i

  logical, save :: flag2 = .true.
  logical :: errorwriteflag

#ifdef CHECK
  complex(kind=prec) :: ps12,ps23,ps34,ps14,ps13,ps24
  complex(kind=prec) :: ms12,ms22,ms32,ms42
  integer :: j
  logical, save :: flag(0:1)=.true.
#endif

100  format(((a)))
111  format(a22,2('(',g24.17,',',g24.17,') ':))
#ifdef CHECK
101  format(a22,g25.17)
  if (argcheck) then
    ms12 = elimminf2_coli(m12)
    ms22 = elimminf2_coli(m22)
    ms32 = elimminf2_coli(m32)
    ms42 = elimminf2_coli(m42)
    ps12 = elimminf2_coli(p12)
    ps23 = elimminf2_coli(p23)
    ps34 = elimminf2_coli(p34)
    ps14 = elimminf2_coli(p14)
    ps24 = elimminf2_coli(p24)
    ps13 = elimminf2_coli(p13)

    if(ms32.eq.rnul.or.ms12.eq.cnul.or.ms22.eq.cnul.or.ms42.eq.cnul
 &      .or.aimag(p12).ne.rnul.or.aimag(p23).ne.rnul
 &      .or.aimag(p34).ne.rnul.or.aimag(p14).ne.rnul
 &      .or.aimag(p24).ne.rnul.or.aimag(p13).ne.rnul) then
      call setErrFlag_coli(-10)
      call ErrOut_coli('D0regrp_coli',' improper arguments',
 &        errorwriteflag)
      if(errorwriteflag) then
        write(nerrout_coli,100)
 &          ' D0regrp_coli called improperly:   (3.61)'
        write(nerrout_coli,111)' D0regrp_coli: p12 = ',p12
        write(nerrout_coli,111)' D0regrp_coli: p23 = ',p23
        write(nerrout_coli,111)' D0regrp_coli: p34 = ',p34
        write(nerrout_coli,111)' D0regrp_coli: p14 = ',p14
        write(nerrout_coli,111)' D0regrp_coli: p13 = ',p13
        write(nerrout_coli,111)' D0regrp_coli: p24 = ',p24
        write(nerrout_coli,111)' D0regrp_coli: m12 = ',m12
        write(nerrout_coli,111)' D0regrp_coli: m22 = ',m22
        write(nerrout_coli,111)' D0regrp_coli: m32 = ',m32
        write(nerrout_coli,111)' D0regrp_coli: m42 = ',m42
        write(nerrout_coli,*)' D0regrp_coli: test= ',
 &          ms32.eq.rnul,ms12.eq.cnul,ms22.eq.cnul,ms42.eq.cnul
 &          ,aimag(p12).ne.rnul,aimag(p23).ne.rnul
 &          ,aimag(p34).ne.rnul,aimag(p14).ne.rnul
 &          ,aimag(p24).ne.rnul,aimag(p13).ne.rnul
      endif
    endif
  endif
#endif

  l12 = (m12+m22-p12)
  l13 = (m12+m32-p13)
  l14 = (m12+m42-p14)
  l23 = (m22+m32-p23)
  l24 = (m22+m42-p24)
  l34 = (m32+m42-p34)

  if(l12.ne.cnul)then
    r12 = l12/(2*m22)*(rone+sqrt(rone-4*m12*m22/l12**2))
  else
    r12 = cima*sqrt(m12/m22)
  endif
  r21 = r12*m22/m12

  if(l13.ne.cnul)then
    if(p13.eq.cnul)then
      r31=cone
    elseif(abs(m32).le.abs(m12))then
      r31 = l13/(2*m12)*(rone+sqrt(rone-4*m12*m32/l13**2))
    else
      r31 = (2*m32)/(l13*(rone+sqrt(rone-4*m12*m32/l13**2)))
    endif
  else
    r31 = cima*sqrt(m32/m12)
  endif

  r13 = r31*m12/m32

  if(l14.ne.cnul)then
    r14 = l14/(2*m42)*(rone+sqrt(rone-4*m12*m42/l14**2))
  else
    r14 = cima*sqrt(m12/m42)
  endif
  r41 = r14*m42/m12
  if(l23.ne.cnul)then
    r23 = l23/(2*m32)*(rone+sqrt(rone-4*m22*m32/l23**2))
  else
    r23 = cima*sqrt(m22/m32)
  endif
  r32 = r23*m32/m22
  if(l24.ne.cnul)then
    r24 = l24/(2*m42)*(rone+sqrt(rone-4*m22*m42/l24**2))
  else
    r24 = cima*sqrt(m22/m42)
  endif
  r42 = r24*m42/m22
  if(l34.ne.cnul)then
    r34 = l34/(2*m42)*(rone+sqrt(rone-4*m32*m42/l34**2))
  else
    r34 = cima*sqrt(m32/m42)
  endif
  r43 = r34*m42/m32

  r42 = rone/r24
  r24 = m22/m42/r24

  if( aimag(r31).ne.rnul.and. &
      (aimag(m22*r31*r31).gt.rnul.or.aimag(m42*r31*r31).gt.rnul.or. &
      aimag(r31*l23).gt.rnul.or.aimag(r31*l34).gt.rnul.or. &
      aimag(r31*r31*l24).gt.rnul)) then
    r31 = rone/r13
    r13 = m12/m32/r13
  endif

  ir12 = sign(rone,real((r12*r21-rone)*r12*m12))
  ir13 = sign(rone,real((r13*r31-rone)*r13*m12))
  ir14 = sign(rone,real((r14*r41-rone)*r14*m12))
  ir23 = sign(rone,real((r23*r32-rone)*r23*m22))
  ir24 = sign(rone,real((r24*r42-rone)*r24*m22))
  ir34 = sign(rone,real((r34*r43-rone)*r34*m32))

  a   =  m42*(l34/r42-l23 + (l12-l14/r42)*r31)
  b   =  m12*m42*(rone/r13-r31)*(rone/r42-r24)+l12*l34-l14*l23
  c   =  m12*(l34*r24-l23 + (l12-l14*r24)/r13)
  d   =  l23-r31*l12 - (l34-r31*l14)*r24
  det =  sqrt(b*b-4*a*c)

! calculation of det via chdet implemented 23.11.2017
  if(abs(det)**2.lt.sqrt(calacc)*max(abs(b*b),abs(4*a*c))) then
    yc(1,2) = l12
    yc(1,3) = l13
    yc(1,4) = l14
    yc(2,3) = l23
    yc(2,4) = l24
    yc(3,4) = l34
    yc(2,1) = l12
    yc(3,1) = l13
    yc(4,1) = l14
    yc(3,2) = l23
    yc(4,2) = l24
    yc(4,3) = l34
    yc(1,1) = 2*m12
    yc(2,2) = 2*m22
    yc(3,3) = 2*m32
    yc(4,4) = 2*m42
    det = sqrt(chdet(4,yc))
  endif

  if(abs(a).lt.calacc*abs(m42)* &
        max(abs(l34/r42),abs(l23),abs(l12*r31),abs(l14/r42*r31)) &
      .or.abs(det)**2.lt.calacc**2*max(abs(b*b),abs(4*a*c))) then
! changed 23.11.2017
!     &    .or.abs(det)**2.lt.calacc*max(abs(b*b),abs(4*a*c))) then
    !call setErrFlag_coli(-7)
    !call ErrOut_coli('D0regrp_coli','case not implemented', &
    !    errorwriteflag)
    !if (errorwriteflag) then
    !  write(nerrout_coli,100) &
    !         ' D0regrp_coli: singularity encountered'
    !  if (abs(a).lt.calacc*abs(m42)* &
    !      max(abs(l34/r42),abs(l23),abs(l12*r31),abs(l14/r42*r31))) &
    !  then
    !    write(nerrout_coli,111)' D0regrp_coli: 0=a = ',a
    !  endif
    !  if (abs(det)**2.lt.calacc*max(abs(b*b),abs(4*a*c))) then
    !    write(nerrout_coli,111) &
    !         ' D0regrp_coli: det =  ',det,b*b,4*a*c
    !  endif
    !  write(nerrout_coli,111)' D0regrp_coli: p12 =  ',p12
    !  write(nerrout_coli,111)' D0regrp_coli: p23 =  ',p23
    !  write(nerrout_coli,111)' D0regrp_coli: p34 =  ',p34
    !  write(nerrout_coli,111)' D0regrp_coli: p14 =  ',p14
    !  write(nerrout_coli,111)' D0regrp_coli: p24 =  ',p24
    !  write(nerrout_coli,111)' D0regrp_coli: p13 =  ',p13
    !  write(nerrout_coli,111)' D0regrp_coli: m12 =  ',m12
    !  write(nerrout_coli,111)' D0regrp_coli: m22 =  ',m22
    !  write(nerrout_coli,111)' D0regrp_coli: m32 =  ',m32
    !  write(nerrout_coli,111)' D0regrp_coli: m42 =  ',m42
    !  write(nerrout_coli,*)
    !endif
    D0regrp_coli = undefined
    if (abs(a).eq.rnul.or.abs(det).eq.rnul) return
  endif

  x(1,4) = (-b+sqrt(b*b-4*a*c))/(2*a)
  x(2,4) = (-b-sqrt(b*b-4*a*c))/(2*a)
  if(abs(x(1,4)).gt.abs(x(2,4))) then
    x(2,4) = c/(a*x(1,4))
  else
    x(1,4) = c/(a*x(2,4))
  endif

  ix(1,4) = -sign(rone,real(d))
  ix(2,4) = +sign(rone,real(d))
  ix(1,1) =  sign(rone,ix(1,4)*real(r24)) ! needed for correct
  ix(2,1) =  sign(rone,ix(2,4)*real(r24)) ! continuation

  x(1,1) = x(1,4)/r24
  x(2,1) = x(2,4)/r24
  D0regrp_coli = dcmplx(rnul)
  do i=1,2
    eta=eta2s_coli(-x(i,4),rone/r24,-ix(i,4),-ir24,-ix(i,1))

    if(eta.ne.rnul)then
      ch0   = m42*(r31*x(i,4))**2+l34*r31*x(i,4)+m32
      test0 = abs(ch0)/max(abs(m42*(r31*x(i,4))**2), &
          abs(l34*r31*x(i,4)),abs(m32))
      ch1   = m42*(x(i,4))**2+l14*x(i,4)+m12
      test1 = abs(ch1)/max(abs(m42*(x(i,4))**2),abs(l14*x(i,4)),abs(m12))
      test01=min(test0,test1)
      ch2   = m22*(r31*x(i,1))**2+l23*r31*x(i,1)+m32
      test2 = abs(ch2)/max(abs(m22*(r31*x(i,1))**2),abs(l23*r31*x(i,1)),abs(m32))
      ch3   = m22*(x(i,1))**2+l12*x(i,1)+m12
      test3 = abs(ch3)/max(abs(m22*(x(i,1))**2),abs(l12*x(i,1)),abs(m12))
      test23 = min(test2,test3)

      ch4   = (m42*r31*x(i,4)*(rone/r42-r24)+(l23-r24*l34))*r31
      test4 = abs(ch4)/max(abs(m42*r31*r31*x(i,4)/r42), &
          abs(m42*r31*r31*x(i,4)*r24),abs(l23*r31), &
          abs(r31*r24*l34))
      ch5   = m42*x(i,4)*(rone/r42-r24)+(l12-r24*l14)
      test5 = abs(ch5)/max(abs(m42*x(i,4)/r42), &
          abs(m42*x(i,4)*r24),abs(l12),abs(r24*l14))
      test45 = min(test2,test3)

      if (test23.gt.test01.and.test23.gt.test45) then
        argl1 = ch2/ch3
      elseif(test01.gt.test45)then
        argl1 = ch0/ch1
      else
        argl1 = ch4/ch5
      endif

      if(abs(aimag(argl1)).lt.1d1*impacc*abs(argl1)) then

        if(abs(aimag(r24)).gt.impacc*abs(r24))then
          v=aimag(x(i,4))/aimag(r24)
          u=aimag(x(i,4)/r24)/aimag(rone/r24)
          ipop(i) = real(-m12-m22*v*v-m42*u*u-l12*v-l14*u-l24*u*v)
        else                ! imaginary part results only from x4(i)
          ipop(i) = real(m42*(r24-rone/r42)*ix(i,4)*d*r31)
        endif
      else
        ipop(i) = rnul
      endif

      l1 = cln_coli(argl1,ipop(i))

      if(eta.ne.rnul)then
        D0regrp_coli = D0regrp_coli + (2*i-3) * ( &
            + eta*(l1-cln_coli(m32/m12,real(m32-m12))) )
      endif

    endif

    D0regrp_coli = D0regrp_coli + (2*i-3) * (           &
        - cspcos_coli(-x(i,4),r43*r31,-ix(i,4),ir34)    &
        - cspcos_coli(-x(i,4),r31/r34,-ix(i,4),-ir34)   &
        + cspcos_coli(-x(i,4),r41,-ix(i,4),ir14)        &
        + cspcos_coli(-x(i,4),rone/r14,-ix(i,4),-ir14)  &
        - cspcos_coli(-x(i,1),r21,-ix(i,1),ir12)        &
        - cspcos_coli(-x(i,1),rone/r12,-ix(i,1),-ir12)  &
        + cspcos_coli(-x(i,1),r31*r23,-ix(i,1),ir23)    &
        + cspcos_coli(-x(i,1),r31/r32,-ix(i,1),-ir23))

  enddo

  D0regrp_coli = D0regrp_coli/det

  end

!***********************************************************************
  function D0comb_coli(p12,p23,p34,p14,p13,p24,m12,m22,m32,m42)
!***********************************************************************
!  general scalar 4-point function                                     *
!  regular case based on propagator identity                           *
!  reduces result to 2 4-point function with one vanishing pij         *
!                                                                      *
!                     m22                                              *
!       p12  ---------------------  p23                                *
!                 |    2    |                                          *
!                 |         |                                          *
!              m12| 1     3 | m32                                      *
!                 |         |                                          *
!                 |    4    |                                          *
!       p14  ---------------------  p34                                *
!                     m42                                              *
!                                                                      *
!----------------------------------------------------------------------*
!  09.03.10 Ansgar Denner       last changed 27.11.17 Ansgar Denner    *
!***********************************************************************
  implicit none
  complex(kind=prec) :: p12,p23,p34,p14,p13,p24
  complex(kind=prec) :: m12,m22,m32,m42
  complex(kind=prec) :: q2(0:3,0:3),m2(0:3)
  complex(kind=prec) :: D0comb_coli

  real(kind=prec)    :: lambda,ga,omga,gas,omgas,gasomgas
  complex(kind=prec) :: mm2,qq2,D01,D02
  integer :: i,j,k,l,i1,i2,i3,i4
  logical :: errorwriteflag

#ifdef CHECK
  complex(kind=prec) :: ps12,ps23,ps34,ps14,ps13,ps24
  complex(kind=prec) :: ms12,ms22,ms32,ms42
#endif

111  format(a22,2('(',g24.17,',',g24.17,') ':))

  D0comb_coli = undefined

#ifdef CHECK
100  format(((a)))
101  format(a22,g25.17)
  if (argcheck) then
    ms12 = elimminf2_coli(m12)
    ms22 = elimminf2_coli(m22)
    ms32 = elimminf2_coli(m32)
    ms42 = elimminf2_coli(m42)
    ps12 = elimminf2_coli(p12)
    ps23 = elimminf2_coli(p23)
    ps34 = elimminf2_coli(p34)
    ps14 = elimminf2_coli(p14)
    ps24 = elimminf2_coli(p24)
    ps13 = elimminf2_coli(p13)

    if(ms32.eq.rnul.or.ms12.eq.cnul.or.ms22.eq.cnul.or.ms42.eq.cnul
 &      .or.aimag(p12).ne.rnul.or.aimag(p23).ne.rnul
 &      .or.aimag(p34).ne.rnul.or.aimag(p14).ne.rnul
 &      .or.aimag(p24).ne.rnul.or.aimag(p13).ne.rnul) then
      call setErrFlag_coli(-10)
      call ErrOut_coli('D0comb_coli',' improper arguments',
 &        errorwriteflag)
      if (errorwriteflag) then
        write(nerrout_coli,100)' D0comb_coli called improperly:'
        write(nerrout_coli,111)' D0comb_coli: p12 = ',p12
        write(nerrout_coli,111)' D0comb_coli: p23 = ',p23
        write(nerrout_coli,111)' D0comb_coli: p34 = ',p34
        write(nerrout_coli,111)' D0comb_coli: p14 = ',p14
        write(nerrout_coli,111)' D0comb_coli: p13 = ',p13
        write(nerrout_coli,111)' D0comb_coli: p24 = ',p24
        write(nerrout_coli,111)' D0comb_coli: m12 = ',m12
        write(nerrout_coli,111)' D0comb_coli: m22 = ',m22
        write(nerrout_coli,111)' D0comb_coli: m32 = ',m32
        write(nerrout_coli,111)' D0comb_coli: m42 = ',m42
      endif
    endif
  endif
#endif

  q2(0,1)=p12
  q2(1,0)=p12
  q2(0,2)=p13
  q2(2,0)=p13
  q2(0,3)=p14
  q2(3,0)=p14
  q2(2,1)=p23
  q2(1,2)=p23
  q2(3,1)=p24
  q2(1,3)=p24
  q2(2,3)=p34
  q2(3,2)=p34
  m2(0)=m12
  m2(1)=m22
  m2(2)=m32
  m2(3)=m42

  do i=0,2
  do j=i+1,3
  if(j.ne.i)then
    if(q2(i,j).eq.rnul)then
      i1=i
      i3=j
      if(mod(i3-i1,2).eq.0)then
        i2=mod(i1+1,4)
        i4=mod(i1+3,4)
      elseif(mod(i3-i1,4).eq.1)then
        i2=mod(i3+1,4)
        i4=mod(i3+2,4)
      else
        i2=mod(i1+1,4)
        i4=mod(i1+2,4)
      endif

      D0comb_coli=D0regrp_coli(q2(i1,i2),q2(i2,i3),q2(i3,i4), &
          q2(i4,i1),q2(i1,i3),q2(i2,i4),m2(i1),m2(i2),m2(i3),m2(i4))

      return

    endif
  endif
  enddo
  enddo

  i1=-1
  i2=-1
  i3=-1
  gasomgas = rnul
  ga = rnul
  omga = rnul
  do i=0,3
  do j=0,3
  if(j.ne.i)then
  do k=0,3
  if(k.ne.i.and.k.ne.j)then
    if(real(q2(i,j)).ge.rnul.and.real(q2(i,k)).ge.rnul)then
      lambda=real((q2(i,j)-q2(i,k))**2-2*q2(j,k)*(q2(i,j)+q2(i,k)) &
        +q2(j,k)**2)
      if(lambda.ge.rnul)then
        if(real(q2(i,j)-q2(i,k)+q2(j,k)).gt.rnul)then
          ga=real((q2(i,j)-q2(i,k)+q2(j,k)+sqrt(lambda))/(2*q2(i,j)))
        else
          ga=real(2*q2(j,k)/(q2(i,j)-q2(i,k)+q2(j,k)-sqrt(lambda)))

        endif
        if(real(q2(i,j)+q2(i,k)-q2(j,k)).lt.rnul)then
          omga=real((q2(i,j)+q2(i,k)-q2(j,k)-sqrt(lambda))/(2*q2(i,j)))

        else
          omga=real(2*q2(i,k)/(q2(i,j)+q2(i,k)-q2(j,k)+sqrt(lambda)))

        endif

      else
        !call setErrFlag_coli(-10)
        !call ErrOut_coli('D0comb_coli', &
        !     ' improper call: case 1, lambda<0', &
        !    errorwriteflag)
        !if (errorwriteflag) then
        !  write(nerrout_coli,*) &
        !      'D0comb_coli: case 1, lambda<0 =',lambda
        !  write(nerrout_coli,*) 'D0comb_coli: ijk ',i,j,k
        !  write(nerrout_coli,*) 'D0comb_coli: qijk ', &
        !      q2(i,j),q2(i,k),q2(j,k)
        !  write(nerrout_coli,111)' D0comb_coli: p12 = ',p12
        !  write(nerrout_coli,111)' D0comb_coli: p23 = ',p23
        !  write(nerrout_coli,111)' D0comb_coli: p34 = ',p34
        !  write(nerrout_coli,111)' D0comb_coli: p14 = ',p14
        !  write(nerrout_coli,111)' D0comb_coli: p13 = ',p13
        !  write(nerrout_coli,111)' D0comb_coli: p24 = ',p24
        !  write(nerrout_coli,111)' D0comb_coli: m12 = ',m12
        !  write(nerrout_coli,111)' D0comb_coli: m22 = ',m22
        !  write(nerrout_coli,111)' D0comb_coli: m32 = ',m32
        !  write(nerrout_coli,111)' D0comb_coli: m42 = ',m42
        !endif
      endif
    elseif(real(q2(i,j)).lt.rnul.and.real(q2(i,k)).lt.rnul &
          .and.real(q2(j,k)).gt.rnul)then
      lambda=real((q2(i,j)-q2(i,k))**2-2*q2(j,k)*(q2(i,j)+q2(i,k))+q2(j,k)**2)
      if(lambda.ge.rnul)then
        if(real(q2(i,j)-q2(i,k)+q2(j,k)).lt.rnul)then
          ga=real((q2(i,j)-q2(i,k)+q2(j,k)-sqrt(lambda))/(2*q2(i,j)))
        else
          ga=real(2*q2(j,k)/(q2(i,j)-q2(i,k)+q2(j,k)+sqrt(lambda)))
        endif
        if(real(q2(i,j)+q2(i,k)-q2(j,k)).gt.rnul)then
          omga=real((q2(i,j)+q2(i,k)-q2(j,k)+sqrt(lambda))/(2*q2(i,j)))
        else
          omga=real(2*q2(i,k)/(q2(i,j)+q2(i,k)-q2(j,k)-sqrt(lambda)))
        endif

      else
 !       call setErrFlag_coli(-10)
 !       call ErrOut_coli('D0comb_coli', &
 !            ' improper call: case 2, lambda<0', &
 !           errorwriteflag)
 !       if (errorwriteflag) then
 !         write(nerrout_coli,*) &
 !             'D0comb_coli: case 2, lambda<0 =',lambda
 !         write(nerrout_coli,*) 'D0comb_coli: ijk ',i,j,k
 !         write(nerrout_coli,*) 'D0comb_coli: qijk ', &
 !             q2(i,j),q2(i,k),q2(j,k)
 !         write(nerrout_coli,111)' D0comb_coli: p12 = ',p12
 !         write(nerrout_coli,111)' D0comb_coli: p23 = ',p23
 !         write(nerrout_coli,111)' D0comb_coli: p34 = ',p34
 !         write(nerrout_coli,111)' D0comb_coli: p14 = ',p14
 !         write(nerrout_coli,111)' D0comb_coli: p13 = ',p13
 !         write(nerrout_coli,111)' D0comb_coli: p24 = ',p24
 !         write(nerrout_coli,111)' D0comb_coli: m12 = ',m12
 !         write(nerrout_coli,111)' D0comb_coli: m22 = ',m22
 !         write(nerrout_coli,111)' D0comb_coli: m32 = ',m32
 !         write(nerrout_coli,111)' D0comb_coli: m42 = ',m42
 !       endif
      endif

    endif

! select variant that maximises ga*omga
    if (gasomgas.lt.ga*omga) then
      gasomgas = ga*omga
      gas = ga
      omgas = omga
      i1 = i
      i2 = j
      i3 = k
    end if
  endif
  enddo
  endif
  enddo
  enddo

  if (i1.eq.-1) then
    !call setErrFlag_coli(-10)
    !call ErrOut_coli('D0comb_coli',' INCONSISTENT INPUT: '// &
    !    'no viable case found' &
    !    ,errorwriteflag)
    !if (errorwriteflag) then
    !  write(nerrout_coli,*) 'D0comb_coli: INCONSISTENT INPUT: '
    !  write(nerrout_coli,'(14x,(a))') &
    !      'make sure that momentum invariants that correspond to'
    !  write(nerrout_coli,'(14x,(a))') &
    !   'squared external masses assume their exact numerical value!'
    !  write(nerrout_coli,111)' D0comb_coli: p12 = ',p12
    !  write(nerrout_coli,111)' D0comb_coli: p23 = ',p23
    !  write(nerrout_coli,111)' D0comb_coli: p34 = ',p34
    !  write(nerrout_coli,111)' D0comb_coli: p14 = ',p14
    !  write(nerrout_coli,111)' D0comb_coli: p13 = ',p13
    !  write(nerrout_coli,111)' D0comb_coli: p24 = ',p24
    !  write(nerrout_coli,111)' D0comb_coli: m12 = ',m12
    !  write(nerrout_coli,111)' D0comb_coli: m22 = ',m22
    !  write(nerrout_coli,111)' D0comb_coli: m32 = ',m32
    !  write(nerrout_coli,111)' D0comb_coli: m42 = ',m42
    !endif
  endif

  i4=0
  do l=0,3
    if(l.ne.i1.and.l.ne.i2.and.l.ne.i3) i4=l
  enddo

  ga = gas
  omga = omgas

  if(ga.lt.rnul.or.omga.lt.rnul)then
    !call setErrFlag_coli(-10)
    !call ErrOut_coli('D0comb_coli',' inconsistent gamma', &
    !    errorwriteflag)
    !if (errorwriteflag) then
    !  write(nerrout_coli,*) &
    !      'D0comb_coli: inconsistent gamma =',ga,omga,lambda
    !  write(nerrout_coli,*) 'D0comb_coli: ijk ',i1,i2,i3
    !  write(nerrout_coli,*) 'D0comb_coli: qijk ', &
    !      q2(i1,i2),q2(i1,i3),q2(i2,i3)
    !  write(nerrout_coli,111)' D0reg_coli: p12 = ',p12
    !  write(nerrout_coli,111)' D0reg_coli: p23 = ',p23
    !  write(nerrout_coli,111)' D0reg_coli: p34 = ',p34
    !  write(nerrout_coli,111)' D0reg_coli: p14 = ',p14
    !  write(nerrout_coli,111)' D0reg_coli: p13 = ',p13
    !  write(nerrout_coli,111)' D0reg_coli: p24 = ',p24
    !  write(nerrout_coli,111)' D0reg_coli: m12 = ',m12
    !  write(nerrout_coli,111)' D0reg_coli: m22 = ',m22
    !  write(nerrout_coli,111)' D0reg_coli: m32 = ',m32
    !  write(nerrout_coli,111)' D0reg_coli: m42 = ',m42
    !endif
  endif

  if(ga.lt.calacc) then
    mm2=(m2(i2)-q2(i2,i3))
    qq2=(q2(i2,i4)-q2(i2,i3))

    if (mm2.ne.cnul) then
      D02= D0regrp_coli(q2(i3,i1), &
          q2(i1,i2),qq2,q2(i4,i3), &
          cnul,q2(i1,i4),m2(i3),m2(i1),mm2,m2(i4))
    else
      D02= D0m0_coli(q2(i3,i1),   &
          q2(i1,i2),qq2,q2(i4,i3),&
          cnul,q2(i1,i4),m2(i3),m2(i1),mm2,m2(i4))
    endif

    D0comb_coli=D02

  else if (omga.lt.calacc) then
    mm2=(m2(i1)-q2(i1,i3))
    qq2=(q2(i1,i4)-q2(i1,i3))

    if (mm2.ne.cnul) then
      D01= D0regrp_coli(q2(i3,i2),q2(i2,i1),qq2, &
          q2(i4,i3),cnul,q2(i2,i4),m2(i3),m2(i2),mm2,m2(i4))
    else
      D01= D0m0_coli(q2(i3,i2),q2(i2,i1),qq2, &
          q2(i4,i3),cnul,q2(i2,i4),m2(i3),m2(i2),mm2,m2(i4))
    endif

    D0comb_coli=D01

  else
    mm2=ga*(m2(i1)-q2(i1,i3)) + omga*(m2(i2)-q2(i2,i3))
    qq2=ga*(q2(i1,i4)-q2(i1,i3)) + omga*(q2(i2,i4)-q2(i2,i3))

    if (mm2.ne.cnul) then
      D01= D0regrp_coli(q2(i3,i2),q2(i2,i1)*ga*ga,qq2, &
          q2(i4,i3),cnul,q2(i2,i4),m2(i3),m2(i2),mm2,m2(i4))
      D02= D0regrp_coli(q2(i3,i1),          &
          omga*omga*q2(i1,i2),qq2,q2(i4,i3),&
          cnul,q2(i1,i4),m2(i3),m2(i1),mm2,m2(i4))
    else
      D01= D0m0_coli(q2(i3,i2),q2(i2,i1)*ga*ga,qq2, &
          q2(i4,i3),cnul,q2(i2,i4),m2(i3),m2(i2),mm2,m2(i4))
      D02= D0m0_coli(q2(i3,i1), &
          omga*omga*q2(i1,i2),qq2,q2(i4,i3), &
          cnul,q2(i1,i4),m2(i3),m2(i1),mm2,m2(i4))
    endif

    D0comb_coli=ga*D01+omga*D02

  endif

  end

!***********************************************************************
  function D0m0_coli(p12,p23,p34,p14,p13,p24,m12,m22,m32,m42)
!***********************************************************************
!  scalar 4-point function  for m32 = 0                                *
!  regular case                                                        *
!                                                                      *
!                     m22                                              *
!       p12  ---------------------  p23                                *
!                 |    2    |                                          *
!                 |         |                                          *
!              m12| 1     3 | rnul                                      *
!                 |         |                                          *
!                 |    4    |                                          *
!       p14  ---------------------  p34                                *
!                     m42                                              *
!                                                                      *
!----------------------------------------------------------------------*
!  29.03.92 Ansgar Denner       last changed  14.05.10 Ansgar Denner   *
!                      Bug in checks removed  21.01.13 Ansgar Denner   *
!***********************************************************************
  implicit none
  complex(kind=prec) :: D0m0_coli
  complex(kind=prec) :: p12,p23,p34,p14,p13,p24
  complex(kind=prec) :: m12,m22,m32,m42

  complex(kind=prec) :: l12,l13,l14,l23,l24,l34
  complex(kind=prec) :: mm12,mm22,mm42,swap
  real(kind=prec)    :: ir12,ir14,ir24
  real(kind=prec)    :: ix1(2),ix4(2),iqbard(2)
  real(kind=prec)    :: test0,test1,test2,test3,test4,test5
  real(kind=prec)    :: test01,test23,test45
  real(kind=prec)    :: u,v
  complex(kind=prec) :: r12,r14,r24,r21,r41,r42
  complex(kind=prec) :: a,b,c,d,det
  complex(kind=prec) :: x1(2),x4(2)
  complex(kind=prec) :: ch1,ch2,ch3,l1,ch4,ch5,l2,argl1
  complex(kind=prec) :: eta
  complex(kind=prec) :: mat(4,4)
  integer :: i,j
  logical :: errorwriteflag
  logical, save :: flag2 = .true.

#ifdef CHECK
  complex(kind=prec) :: ps12,ps23,ps34,ps14,ps13,ps24
  complex(kind=prec) :: ms12,ms22,ms32,ms42
  complex(kind=prec) :: D0m0_check
#endif

100  format(((a)))
111  format(a22,2('(',g24.17,',',g24.17,') ':))
#ifdef CHECK
101  format(a22,g25.17)
  if (argcheck) then
    ms12 = elimminf2_coli(m12)
    ms22 = elimminf2_coli(m22)
    ms32 = elimminf2_coli(m32)
    ms42 = elimminf2_coli(m42)
    ps12 = elimminf2_coli(p12)
    ps23 = elimminf2_coli(p23)
    ps34 = elimminf2_coli(p34)
    ps14 = elimminf2_coli(p14)
    ps24 = elimminf2_coli(p24)
    ps13 = elimminf2_coli(p13)

    if(m32.ne.rnul.or.m12.eq.cnul.or.m22.eq.cnul.or.m42.eq.cnul &
        .or.p23.eq.m22.and.p13.eq.m12                       &
        .or.p23.eq.m22.and.p34.eq.m42                       &
        .or.p13.eq.m12.and.p34.eq.m42                       &
        .or.ps23.eq.cnul.and.ms22.eq.cnul                     &
        .or.ps34.eq.cnul.and.ms42.eq.cnul                     &
        .or.ps13.eq.cnul.and.ms12.eq.cnul                     &
        .or.ps12.eq.cnul.and.ms12.eq.cnul.and.ms22.eq.cnul     &
        .or.ps24.eq.cnul.and.ms22.eq.cnul.and.ms22.eq.cnul     &
        .or.aimag(p12).ne.rnul.or.aimag(p23).ne.rnul          &
        .or.aimag(p34).ne.rnul.or.aimag(p14).ne.rnul          &
        .or.aimag(p24).ne.rnul.or.aimag(p13).ne.rnul) then
 !     call setErrFlag_coli(-10)
 !     call ErrOut_coli('D0m0_coli',' wrong arguments',
 !&        errorwriteflag)
 !     if (errorwriteflag) then
 !       write(nerrout_coli,100)' D0m0_coli called improperly:'
 !       write(nerrout_coli,111)' D0m0_coli: p12 = ',p12
 !       write(nerrout_coli,111)' D0m0_coli: p23 = ',p23
 !       write(nerrout_coli,111)' D0m0_coli: p34 = ',p34
 !       write(nerrout_coli,111)' D0m0_coli: p14 = ',p14
 !       write(nerrout_coli,111)' D0m0_coli: p13 = ',p13
 !       write(nerrout_coli,111)' D0m0_coli: p24 = ',p24
 !       write(nerrout_coli,111)' D0m0_coli: m12 = ',m12
 !       write(nerrout_coli,111)' D0m0_coli: m22 = ',m22
 !       write(nerrout_coli,111)' D0m0_coli: m32 = ',m32
 !       write(nerrout_coli,111)' D0m0_coli: m42 = ',m42
 !       write(nerrout_coli,*)' D0m0_coli: test= ',
 !&          m32.ne.rnul,m12.eq.cnul,m22.eq.cnul,m42.eq.cnul,
 !&          p23.eq.m22.and.p13.eq.m12
 !&          ,p23.eq.m22.and.p34.eq.m42
 !&          ,p13.eq.m12.and.p34.eq.m42
 !&          ,ps23.eq.cnul.and.ms22.eq.cnul
 !&          ,ps34.eq.cnul.and.ms42.eq.cnul
 !&          ,ps13.eq.cnul.and.ms12.eq.cnul
 !&          ,ps12.eq.cnul.and.ms12.eq.cnul.and.ms22.eq.cnul
 !&          ,ps14.eq.cnul.and.ms12.eq.cnul.and.ms42.eq.cnul
 !&          ,ps24.eq.cnul.and.ms22.eq.cnul.and.ms22.eq.cnul
 !&          ,aimag(p12).ne.rnul,aimag(p23).ne.rnul
 !&          ,aimag(p34).ne.rnul,aimag(p14).ne.rnul
 !&          ,aimag(p24).ne.rnul,aimag(p13).ne.rnul
 !     endif
    endif
  endif
#endif


!     (permutation 1 -> 3 -> 2 -> 4 -> 1 for  m -> k )
!        m1 -> m2 in normalization

  mm12=m12
  mm22=m22
  mm42=m42

  l12 = (mm22+mm12-p12)
  l13 = (mm12    -p13)
  l14 = (mm42+mm12-p14)
  l23 = (mm22    -p23)
  l24 = (mm22+mm42-p24)
  l34 = (    mm42-p34)

  if(l12.ne.cnul)then
    r12 = l12/(rtwo*mm22)*(rone+sqrt(rone-4*mm12*mm22/l12**2))
  else
    r12 =  cima*sqrt(mm12/mm22)
  endif
  r21 = r12*mm22/mm12


  if (abs(l23*l14).lt.abs(l12*l34)) then
    if(l14.ne.cnul)then
      r14 = l14/(rtwo*mm42)*(rone+sqrt(rone-4*mm12*mm42/l14**2))
    else
      r14 = cima*sqrt(mm12/mm42)
    endif
    if(l24.ne.cnul)then
      r24 = l24/(rtwo*mm42)*(rone+sqrt(rone-4*mm22*mm42/l24**2))
    else
      r24 = cima*sqrt(mm22/mm42)
    endif
  else
    if(l14.ne.cnul)then
      r14 = rtwo*mm12/(l14*(rone+sqrt(rone-4*mm12*mm42/l14**2)))
    else
      r14 = -cima*sqrt(mm12/mm42)
    endif
    if(l24.ne.cnul)then
      r24 = rtwo*mm22/(l24*(rone+sqrt(rone-4*mm22*mm42/l24**2)))
    else
      r24 = -cima*sqrt(mm22/mm42)
    endif
  endif
  r41 = r14*mm42/mm12
  r42 = r24*mm42/mm22

  r24 = 1/r42
  r42 = r24*mm42/mm22

  a =  mm42*(l34/r42 - l23)

! swap if a=0
  if(a.eq.cnul)then
    mm12=m22
    mm22=m12
    swap=l23
    l23=l13
    l13=swap
    swap=l24
    l24=l14
    l14=swap
    swap=r12
    r12=r21
    r21=swap
    swap=r24
    r24=r14
    r14=swap
    swap=r42
    r42=r41
    r41=swap
    a = mm42*(l34/r42 - l23)
  endif

  if(a.eq.cnul)then
    mm12=m42
    mm42=m12
    swap=l34
    l34=l13
    l13=swap
    swap=l24
    l24=l12
    l12=swap
    swap=r14
    r14=r41
    r41=swap
    swap=r24
    r24=r12
    r12=swap
    swap=r42
    r42=r21
    r21=swap
    a = mm42*(l34/r42 - l23)
  endif

  if(real(l12).lt.-rnul) then
    ir12 = sign(10*rone,rone-abs(r12*r12*mm22/mm12))
  else
    ir12 = rnul
  endif
  if(real(l14).lt.-rnul) then
    ir14 = sign(10*rone,rone-abs(r14*r14*mm42/mm12))
  else
    ir14 = rnul
  endif
  if(real(l24).lt.-rnul) then
    ir24 = sign(10*rone,rone-abs(r24*r24*mm42/mm22))
  else
    ir24 = rnul
  endif

  if(abs(a).lt.calacc*abs(mm42)*max(abs(l34/r42),abs(l23))) then
 !   call setErrFlag_coli(-7)
 !   call ErrOut_coli('D0m0_coli','case not implemented',
 !&        errorwriteflag)
 !   if (errorwriteflag) then
 !     write(nerrout_coli,100)' D0m0_coli: case not implemented'
 !     write(nerrout_coli,100)' a = 0 '
 !     write(nerrout_coli,111)' D0m0_coli: p12 = ',p12
 !     write(nerrout_coli,111)' D0m0_coli: p23 = ',p23
 !     write(nerrout_coli,111)' D0m0_coli: p34 = ',p34
 !     write(nerrout_coli,111)' D0m0_coli: p14 = ',p14
 !     write(nerrout_coli,111)' D0m0_coli: p24 = ',p24
 !     write(nerrout_coli,111)' D0m0_coli: p13 = ',p13
 !     write(nerrout_coli,111)' D0m0_coli:mm12 = ',mm12
 !     write(nerrout_coli,111)' D0m0_coli:mm22 = ',mm22
 !     write(nerrout_coli,111)' D0m0_coli: m32 = ',m32
 !     write(nerrout_coli,111)' D0m0_coli:mm42 = ',mm42
 !   endif
    D0m0_coli = undefined
    if (a.eq.cnul) return
  endif

  b   =  l13*mm22*(rone/r24-r42) + l12*l34 -l14*l23
  c   =  l13*(l12-r24*l14) - mm12*l23 + mm12*r24*l34
  d   =  l23 - r24*l34
  det =  sqrt(                                                       &
      l12*l12*l34*l34 + l14*l14*l23*l23 + l24*l24*l13*l13            &
      - rtwo*(l12*l23*l34*l14 + l12*l24*l34*l13 + l14*l24*l23*l13)    &
      + 4*(mm42*l12*l23*l13 + mm12*l23*l34*l24 + mm22*l14*l34*l13) &
      - 4*(mm12*mm42*l23*l23 + mm12*mm22*l34*l34                   &
      + mm22*mm42*l13*l13))

! added 27.07.2018
  if (det.eq.rnul) then
    mat(1,1) = 2*mm12
    mat(2,1) = l12
    mat(3,1) = l13
    mat(4,1) = l14
    mat(1,2) = l12
    mat(2,2) = 2*mm22
    mat(3,2) = l23
    mat(4,2) = l24
    mat(1,3) = l13
    mat(2,3) = l23
    mat(3,3) = 2*m32
    mat(4,3) = l34
    mat(4,4) = 2*mm42
    mat(1,4) = l14
    mat(2,4) = l24
    mat(3,4) = l34

    det = chdet(4,mat)
  end if

  x4(1) = (-b+det)/(rtwo*a)
  x4(2) = (-b-det)/(rtwo*a)
  if(abs(x4(1)).gt.abs(x4(2))) then
    x4(2) = c/(a*x4(1))
  else
    x4(1) = c/(a*x4(2))
  endif
  x1(1) = x4(1)/r24
  x1(2) = x4(2)/r24
  ix4(1) = -sign(rone,real(d))
  ix4(2) = +sign(rone,real(d))
  ix1(1) =  sign(rone,ix4(1)*real(r24))
  ix1(2) =  sign(rone,ix4(2)*real(r24))

  if (abs(l13+x4(1)*l34).lt.acc*abs(l13).and.l13.ne.rnul       &
      .or.abs(l13+x4(2)*l34).lt.acc*abs(l13).and.l13.ne.rnul   &
      .or.abs(l13+x1(1)*l23).lt.acc*abs(l13).and.l13.ne.rnul   &
      .or.abs(l13+x1(2)*l23).lt.acc*abs(l13).and.l13.ne.rnul) then

    swap=mm22
    mm22=mm42
    mm42=swap
    swap=l34
    l34=l23
    l23=swap
    swap=l14
    l14=l12
    l12=swap
    swap=r24
    r24=r42
    r42=swap
    swap=r14
    r14=r12
    r12=swap
    swap=r41
    r41=r21
    r21=swap
    a = mm42*(l34/r42 - l23)

    if(real(l12).lt.-rnul) then
      ir12 = sign(10*rone,rone-abs(r12*r12*mm22/mm12))
    else
      ir12 = rnul
    endif
    if(real(l14).lt.-rnul) then
      ir14 = sign(10*rone,rone-abs(r14*r14*mm42/mm12))
    else
      ir14 = rnul
    endif
    if(real(l24).lt.-rnul) then
      ir24 = sign(10*rone,rone-abs(r24*r24*mm42/mm22))
    else
      ir24 = rnul
    endif

    b   =  l13*mm22*(rone/r24-r42) + l12*l34 -l14*l23
    c   =  l13*(l12-r24*l14) - mm12*l23 + mm12*r24*l34
    d   =  l23 - r24*l34
    det =  sqrt(                                                     &
        l12*l12*l34*l34 + l14*l14*l23*l23 + l24*l24*l13*l13          &
        - rtwo*(l12*l23*l34*l14 + l12*l24*l34*l13 + l14*l24*l23*l13)  &
        + 4*(mm42*l12*l23*l13 + mm12*l23*l34*l24                   &
        + mm22*l14*l34*l13)                                          &
        - 4*(mm12*mm42*l23*l23 + mm12*mm22*l34*l34                 &
        + mm22*mm42*l13*l13))

    x4(1) = (-b+det)/(rtwo*a)
    x4(2) = (-b-det)/(rtwo*a)
    if(abs(x4(1)).gt.abs(x4(2))) then
      x4(2) = c/(a*x4(1))
    else
      x4(1) = c/(a*x4(2))
    endif
    x1(1) = x4(1)/r24
    x1(2) = x4(2)/r24
    ix4(1) = -sign(rone,real(d))
    ix4(2) = +sign(rone,real(d))
    ix1(1) =  sign(rone,ix4(1)*real(r24))
    ix1(2) =  sign(rone,ix4(2)*real(r24))

  endif


  D0m0_coli = dcmplx(rnul)
  dO i=2,1,-1
    eta=eta2s_coli(-x4(i),rone/r24,-ix4(i),-ir24,-ix1(i))

    if(eta.ne.rnul)then

      test0 = abs(d)/max(abs(l23),abs(l34*r24))
      ch1   = l12-r24*l14-mm22*(r42-rone/r24)*x4(i)
      test1 = abs(ch1)/max(abs(l12),abs(mm22*r42*x4(i)), &
          abs(mm22*r24*l14),abs(1/r24*x4(i)))
      ch2    = mm12/x1(i)+l12+mm22*x1(i)
      test01=min(test0,test1)
      test2 = abs(ch2)/max(abs(mm12/x1(i)),abs(l12),abs(mm22*x1(i)))
      ch3   = l23+l13/x1(i)
      test3 = abs(ch3)/max(abs(l23),abs(l13/x1(i)))
      test23 = min(test2,test3)
      ch4    = mm12/x4(i)+l14+mm42*x4(i)
      test4 = abs(ch4)/max(abs(mm12/x4(i)),abs(l14),abs(mm42*x4(i)))
      ch5   = l34+l13/x4(i)
      test5 = abs(ch5)/max(abs(l34),abs(l13/x4(i)))
      test45 = min(test4,test5)
      if (test23.gt.test01.and.test23.gt.test45) then
        argl1 = ch2/ch3
      elseif(test01.gt.test45) then
        argl1 = ch1/d
      else
        argl1 = ch4/ch5
      endif


      if(abs(aimag(argl1)).lt.10*rone*impacc*abs(argl1)) then

        if(abs(aimag(x4(i))).gt.impacc*abs(x4(i)))then
          if(abs(aimag(r24)).gt.impacc*abs(r24))then
            v=aimag(x4(i))/aimag(r24)
            u=aimag(x4(i)/r24)/aimag(rone/r24)
            iqbard(i) = &
                real(mm12+l12*v+l14*u+mm22*v*v+mm42*u*u+l24*u*v &
                -(l13+l23*v+l34*u))

          else
            iqbard(i) = real(-d)

          endif

        else                ! imaginary part results only from x4(i)
          iqbard(i) =real(-mm22*(r42-rone/r24)*ix4(i)/d)
        endif
      else
          iqbard(i) = rnul
      endif

      l1 = cln_coli(argl1,iqbard(i))

    else
      l1 = undefined

    endif


    if (l13.ne.rnul) then



      D0m0_coli = D0m0_coli + (2*i-3) * (                      &
          cspcos_coli(-x4(i),r41,-ix4(i),ir14)                 &
          + cspcos_coli(-x4(i),rone/r14,-ix4(i),-ir14)          &
          - cspcos_coli(-x4(i),l34/l13,-ix4(i),real(l34-l13))  &
          - cspcos_coli(-x1(i),r21,-ix1(i),ir12)               &
          - cspcos_coli(-x1(i),rone/r12,-ix1(i),-ir12)          &
          + cspcos_coli(-x1(i),l23/l13,-ix1(i),real(l23-l13))  &
          )

      if(eta.ne.cnul)then
        D0m0_coli = D0m0_coli + (2*i-3) * ( &
            - eta*(l1+cln_coli(l13/mm12,-rone)) &
            )
      endif


    else


      D0m0_coli = D0m0_coli + (2*i-3) * (                    &
          cspcos_coli(-x4(i),r41,-ix4(i),ir14)               &
          + cspcos_coli(-x4(i),rone/r14,-ix4(i),-ir14)        &
          - cspcos_coli(-x1(i),r21,-ix1(i),ir12)             &
          - cspcos_coli(-x1(i),rone/r12,-ix1(i),-ir12)        &
          + (cln_coli(-x4(i),-ix4(i)))**2/rtwo                &
          - (cln_coli(-x1(i),-ix1(i)))**2/rtwo                &
          + cln_coli(-x4(i),-ix4(i))*cln_coli(l34/mm12,-rone) &
          - cln_coli(-x1(i),-ix1(i))*cln_coli(l23/mm12,-rone) &
          )

       if(eta.ne.cnul)then
         D0m0_coli = D0m0_coli + (2*i-3) * &
             (- eta* l1)
       endif


    endif
  enddo

  D0m0_coli = D0m0_coli/det

  end

end module coli_d0reg_<T>
