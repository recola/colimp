#ifndef COLIFLOWER_H
#define COLIFLOWER_H

namespace coliflower
{
  template <typename RType>
  struct CType {
    RType re;
    RType im;
  };
}

#import "ctypes.inc"

#ifdef __cplusplus
extern "C" {
#endif

  void init_coliflower();
#import "coliflowerC.proc"
#ifdef __cplusplus
}
#endif


namespace coliflower
{

  inline void init() {
    init_coliflower();
  }
#import "coliflowerCpp.proc"

  
} /* coliflower */ 

#endif /* ifndef COLIFLOWER_H */
