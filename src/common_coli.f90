!***********************************************************************
!*     file common_coli.h                                              *
!*     contains global common blocks for COLI                          *
!*---------------------------------------------------------------------*
!*     04.08.08  Ansgar Denner     last changed  27.03.15              *
!***********************************************************************
#import "prectemplate.inc"

module common_coli_<T>
  use params_coli_<T>, only: prec
  implicit none
! information output switch
  logical :: coliinfo

#ifdef SING
! regularization parameters
  real(kind=prec) :: deltauv,delta2ir,delta1ir,colishiftms2
#endif
! regularization parameters
  logical :: ir_rat_terms=.true.
  real(kind=prec) :: muuv2,muir2

! infinitesimal masses
  integer :: ncoliminf
  complex(kind=prec) :: coliminf(100)
  complex(kind=prec) :: coliminf2(100)
  complex(kind=prec) :: coliminffix(100)
  complex(kind=prec) :: coliminffix2(100)

! scaling of infinitesimal masses
  real(kind=prec) :: coliminfscale,coliminfscale2
end module common_coli_<T>
