program A0test
  use coliflower, only: initcoli,A0,dp,lp,qp
  use test_tools, only: compare
  implicit none

  complex(kind=dp) :: m02_dp
  complex(kind=lp) :: m02_lp
  complex(kind=qp) :: m02_qp

  m02_qp = cmplx(10,0,kind=qp)
  m02_dp = m02_qp
  m02_lp = m02_qp

  call initcoli
  call compare(A0(m02_dp), A0(m02_lp), 15)
  call compare(A0(m02_dp), A0(m02_qp), 15)
  
end program
